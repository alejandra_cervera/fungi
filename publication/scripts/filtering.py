#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: cerverat
"""

import pandas as pd
import sys
import argparse
import os


def get_options():
    parser = argparse.ArgumentParser()
    parser.add_argument("input", help="Input CSV file")
    parser.add_argument("output", help="Output folder name")

    options = parser.parse_args()
    return options


def read_input(in_file):
    df = pd.DataFrame(pd.read_csv(in_file, sep="\t", dtype="str", header=0))

    # keep intersting columns and clean data
    df["Oncofuse_score"] = pd.to_numeric(df["Oncofuse_score"], errors="coerce")
    df["Pegasus_score"] = pd.to_numeric(df["Pegasus_score"], errors="coerce")
    df["DB"] = df["DB"].fillna("")

    # add patient column based on file source
    patient = df["Source"].map(lambda v: v.split("_")[0])
    df["Patient"] = patient
    return df


def create_output(out_dir):
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)


def group(data, out_dir):
    # group by fusion name
    res = data.groupby("FusionName").agg(
        {
            "Pegasus_score": "max",
            "Oncofuse_score": "max",
            "bp_coordinates": ",".join,
            "Source": ",".join,
            "Methods": ",".join,
            "DB": ",".join,
            "Patient": ",".join,
        }
    )

    # remove duplicates in aggregated columns
    res["Methods"] = res["Methods"].apply(
        lambda x: ",".join(sorted(set([y.strip() for y in x.split(",")])))
    )
    res["DB"] = res["DB"].apply(
        lambda x: ",".join(sorted(set([y.strip() for y in x.split(",")])))
    )
    res["Patient"] = res["Patient"].apply(
        lambda x: ",".join(sorted(set([y.strip() for y in x.split(",")])))
    )

    res.to_csv(os.path.join(out_dir, "grouped_fusions.csv"), sep="\t")

    return res


def filt_1(data, out_dir):
    # filter out fusions only called by ericscript
    filt1 = data[data["Methods"] != "ericscript"]

    filt1.to_csv(os.path.join(out_dir, "filt_1_fusions.csv"), sep="\t")
    return filt1


def filt_2(data, out_dir):
    # annotations that do not include cancer databases
    terms = [
        "adjacent,banned",
        "adjacent,banned,cell_lines,known",
        "adjacent,banned,hpa",
        "adjacent,banned,hpa,known",
        "banned",
        "banned,hpa",
        "banned,hpa,known",
    ]

    # exclude fusions based on annotations
    filt2 = data[~data.DB.isin(terms)]
    filt2.to_csv(os.path.join(out_dir, "filt_2_fusions.csv"), sep="\t")
    return filt2


def filt_3(data, out_dir):
    # exclude fusions based on oncogenic scores
    filt3 = data[(data["Pegasus_score"] >= 0.25) | (data["Oncofuse_score"] >= 0.6)]
    filt3.to_csv(os.path.join(out_dir, "filt_3_fusions.csv"), sep="\t")
    return filt3


def filt_final(data, filtered_3, out_dir):
    # filtered fusions keeping original columns plus a Sample column (split from source sample name and method)
    new=data[data.FusionName.isin(list(filtered_3.index))]
    new['Sample'] = new['Source'].str.rsplit('_', n=1).str.get(0)
    new.to_csv(os.path.join(out_dir, "filtered_final_candidates.csv"),sep="\t",index=False)


if __name__ == "__main__":
    opts = get_options()
    create_output(opts.output)
    data = read_input(opts.input)
    grouped = group(data, opts.output)
    filtered_1 = filt_1(grouped, opts.output)
    filtered_2 = filt_2(filtered_1, opts.output)
    filtered_3 = filt_3(filtered_2, opts.output)
    filt_final(data, filtered_3, opts.output)
