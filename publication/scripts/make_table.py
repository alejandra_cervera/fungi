
import pandas as pd
import sys
import os

# concat all statistics from each of the pipeline stages
# each input file has 2 columns, the sample name with method and the count of fusions for each sample at that stage

myDir=sys.argv[1]
outfile=sys.argv[2]

datos = pd.read_csv(os.path.join(myDir,"summary_called"), sep='\t',dtype="str",header=None,index_col=1)
df_called = pd.DataFrame(datos)
df_called.columns=['Called']

datos = pd.read_csv(os.path.join(myDir,"summary_custom"), sep='\t',dtype="str",header=None,index_col=1)
df_custom = pd.DataFrame(datos)
df_custom.columns=['MethodCustomFiltered']

datos = pd.read_csv(os.path.join(myDir,"summary_split"), sep='\t',dtype="str",header=None,index_col=1)
df_split = pd.DataFrame(datos)
df_split.columns=['ReadCountFiltered']

datos = pd.read_csv(os.path.join(myDir,"summary_ensembl"), sep='\t',dtype="str",header=None,index_col=1)
df_ensembl = pd.DataFrame(datos)
df_ensembl.columns=['GeneAnnotFiltered']

datos = pd.read_csv(os.path.join(myDir,"summary_db"), sep='\t',dtype="str",header=None,index_col=1)
df_db = pd.DataFrame(datos)
df_db.columns=['FusionAnnotFiltered']

datos = pd.read_csv(os.path.join(myDir,"summary_scored"), sep='\t',dtype="str",header=None,index_col=1)
df_scored = pd.DataFrame(datos)
df_scored.columns=['ScoredFiltered']

df = pd.concat([df_called,df_custom,df_split,df_ensembl,df_db,df_scored],axis=1)
df1=df.fillna(0)

df1.to_csv(outfile,sep="\t")
