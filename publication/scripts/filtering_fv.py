#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: cerverat

filtered_fv_fusions.csv is the filtering step
formatted_filtered_fv.csv is for input into FusionConsensus

"""

import pandas as pd
import argparse
import os
import csv


def get_options():
    parser = argparse.ArgumentParser()
    parser.add_argument("input", help="Input CSV file")
    parser.add_argument("output", help="Output folder name")

    options = parser.parse_args()
    return options


def create_output(out_dir):
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)


def filter_fv(df, out_dir):
    df["FFPM"] = pd.to_numeric(df["FFPM"], errors="coerce")
    df["JunctionReadCount"] = pd.to_numeric(df["JunctionReadCount"], errors="coerce")

    # filter fusions with rounded FFPM<0.1 & JunctionReadCount <3 keeping all original columns
    df1 = df[df["FFPM"].round(2) >= 0.1]
    df2 = df1[df1["JunctionReadCount"] >= 3]
    df2.to_csv(os.path.join(out_dir, "filtered_fv_fusions.csv"), sep="\t", index=False)
    return df2


def format_filtered(df, out_dir):

    # filtered results now formatted for FusionConsensus --input
    # Left and RightBreakpoint are merged into bp_coordinates format: chrL:bpL>chrR:bpR
    with open(os.path.join(out_dir, "formatted_filtered_fv.csv"), "wt") as fp:
        writer = csv.writer(fp, delimiter="\t")
        writer.writerow(
            [
                "FusionName",
                "Sample",
                "Methods",
                "Encompassing reads",
                "Split reads",
                "bp_coordinates",
            ]
        )

        for i, row in df2.iterrows():
            lbp = row["LeftBreakpoint"].split(":")
            rbp = row["RightBreakpoint"].split(":")
            bp = "{}:{}>{}:{}".format(lbp[0], lbp[1],rbp[0],rbp[1])
            writer.writerow(
                [
                    row["#FusionName"],
                    row["Sample"],
                    "fusioninspector",
                    row["JunctionReadCount"],
                    row["SpanningFragCount"],
                    bp,
                ]
            )


if __name__ == "__main__":
    opts = get_options()
    create_output(opts.output)

    df = pd.DataFrame(pd.read_csv(opts.input, sep="\t", dtype="str", header=0))
    df2 = filter_fv(df, opts.output)
    format_filtered(df2, opts.output)
