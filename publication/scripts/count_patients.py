#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: cerverat


"""

import pandas as pd
import argparse
import os


def get_options():
    parser = argparse.ArgumentParser()
    parser.add_argument("input", help="Input CSV file")
    parser.add_argument("output", help="Output folder name")

    options = parser.parse_args()
    return options


def create_output(out_dir):
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)


def count_patients(in_file, out_dir):
    df = pd.DataFrame(pd.read_csv(in_file, sep="\t", dtype="str", header=0))

    new = {}
    new["Fusion"] = df["FusionName"]
    new["Patient"] = df["Sample"].str.rsplit("_", n=0).str.get(0)
    new["Timepoint"] = df["Sample"].str.rsplit("_", n=0).str.get(1).str[0]
    new_df = pd.DataFrame(data=new)
    data = new_df.groupby(["Fusion"]).agg({"Patient": ",".join, "Timepoint": ",".join})
    data["Patient"] = data["Patient"].apply(
        lambda x: ",".join(sorted(set([y.strip() for y in x.split(",")])))
    )
    data["PatientCount"] = data["Patient"].apply(lambda x: len(x.split(",")))
    data["Timepoint"] = data["Timepoint"].apply(
        lambda x: ",".join(sorted(set([y.strip() for y in x.split(",")])))
    )
    data["TimepointCount"] = data["Timepoint"].apply(lambda x: len(x.split(",")))
    data.to_csv(
        os.path.join(out_dir, "fusions_patient_timepoint_count.csv"),
        sep="\t",
        index=True,
    )


if __name__ == "__main__":
    opts = get_options()
    create_output(opts.output)
    count_patients(opts.input, opts.output)
