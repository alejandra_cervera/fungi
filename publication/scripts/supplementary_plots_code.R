library(grid)
library(gridExtra)
library(ggplot2)
library(UpSetR)
library(reshape2)
library(tibble)
library(ggpmisc)
library(scales)
library(patchwork)
library(UpSetR)

## Supplementary Figure 1
## Fusion counts at each pipeline stage


# visually distinct colors for each method from https://mokole.com/palette.html
#seagreen #2e8b57 orange #ffa500 lime #00ff00 blue #0000ff dodgerblue #1e90ff deeppink #ff1493
myCols=c("#2e8b57","#ffa500","#00ff00","#0000ff","#1e90ff","#ff1493")
# colors for pipeline stages
cls<- c("cadetblue4","grey50","tomato3","tomato3","tomato3","tomato3")

# table created by get_stats.sh, leaving out Sample id column
methods_counts <- read.table("InputFiles/final_methods_fusion_count_table.csv",sep="\t",header = T,stringsAsFactors=F)[,-1]
agg = aggregate(methods_counts[,-1], by = list(methods_counts$Method),sum)
colnames(agg) <- c("method","original calls","custom filter","count filter","gene-level filter","fusion-level filter","scored fusion-bps")
melt_data<-melt(data = agg, 
                id.vars = "method", 
                measure.vars = c("original calls","custom filter","count filter","gene-level filter","fusion-level filter","scored fusion-bps"))

melt_data['color']=rep(myCols2,6)
method_color=melt_data[1:6,c(1,4)]

# prep zoomed section of plot: score filter results for all methods except ericscript
zoom_data_all <- melt_data[which(melt_data$variable == "scored fusion-bps"),]
zoom_data <- zoom_data_all[which(zoom_data_all$method!="ericscript"),]
z<-zoom_data[order(zoom_data$value,decreasing=T),3]

zoom <- ggplot(data=zoom_data, aes(x=variable, y=value, group=method)) +
  geom_line(aes(color=method)) +
  geom_point(aes(color=method)) +
  scale_colour_manual(values=setNames(zoom_data$color, zoom_data$method)) +
  theme(legend.position="none") +
  annotate("text", x = 1, y = z[1]-60, label = z[1],size=2.5) +
  annotate("text", x = 1, y = z[2]+60, label = z[2],size=2.5) +   
  annotate("text", x = 1, y = z[3]+60, label = z[3],size=2.5) +
  annotate("text", x = 1, y = z[4]-60, label = z[4],size=2.5) + 
  annotate("text", x = 1, y = z[5]+60, label = z[5],size=2.5)
  #geom_text(aes(label=value),nudge_x = 0.3, size=2.5) 
zoom
  data.tb <- tibble(x = 7.8, y = 0, plot = list(zoom + 
                                                theme_classic() + 
                                                theme(legend.position = "none", axis.title=element_blank()) +
                                                geom_point(aes(color=method)) ))
                  
# left hand plot
mylabels<-sort(melt_data[1:6,3],decreasing = T)
p1<-ggplot(data=melt_data, aes(x=variable, y=value/1000, group=method)) +
  labs(title=NULL,subtitle="by method", x=NULL,y = "fusions / 1000") +
  #labs(title="Number of fusions at each pipeline step",subtitle="by method", x=NULL,y = "fusions / 1000") +
  scale_x_discrete(labels = wrap_format(15)) +
  geom_line(aes(color=method)) + 
  geom_point(aes(color=method)) +
  scale_colour_manual(values=setNames(melt_data$color, melt_data$method)) +
  theme(legend.position = "none") + 
  geom_plot(data = data.tb, aes(x, y,label=plot,vp.width=0.17,vp.height=1)) +
  theme_classic() +
  theme(legend.position="top",
        legend.justification = c(0,1),
        legend.key.size = unit(0.2,"cm"), 
        legend.key.width = unit(0.2,"cm"),
        plot.title = element_text(hjust = 0.5,size=20), 
        plot.subtitle = element_text(hjust = 0,size=15),
        legend.title = element_blank(), 
        axis.text=element_text(size=12),
        axis.text.x = element_text(angle = 45,hjust=1,colour=cls)) +
  expand_limits(x = 8.2, y=-5) + 
  geom_rect(aes(xmin = 6 - 0.1, xmax = 6 + 0.1, ymin = -2 - 0.5, ymax = 3.5), 
            linetype="dashed", fill = "transparent", color = "grey40", size=0.3) +
  annotate("text", x = 1, y = 125, label =toString(mylabels[1]),size=3) + 
  annotate("text", x = 1, y = 82, label = toString(mylabels[2]),size=3)  +   
  annotate("text", x = 1.1, y = 15, label = toString(mylabels[3]),size=3) + 
  annotate("text", x = 0.7, y = 8, label = toString(mylabels[4]),size=3) + 
  annotate("text", x = 0.7, y = 2, label = toString(mylabels[5]),size=3) + 
  annotate("text", x = 1.1, y = -1.1, label = toString(mylabels[6]),size=3) +
  annotate("text", x = 5.9, y = 15, label = toString(zoom_data_all[which(zoom_data_all$method=="ericscript"),3]),size=3) +
  geom_segment(aes(x = 6.14, y = 5, xend = 6.25, yend = 90),linetype=2, color = "grey40") + 
  geom_segment(aes(x = 6.2, y = -1, xend = 6.5, yend = -1),linetype=2, color = "grey40")
p1 

ggsave("OutputPlots/line_plot.pdf",p1,device="pdf")
ggsave("OutputPlots/line_plot.jpg",p1,device="jpeg")

# right hand plot
fusion_counts <- read.table("InputFiles/supTableStats2_out.csv",sep="\t",header = T,stringsAsFactors=F)
ss <- t(fusion_counts)
rownames(ss) <- c("scored fusions","custom filter","supervised match")
df<-data.frame(Filt=rownames(ss),Counts=ss[,1],row.names = NULL)
df$Filt <- factor(df$Filt, levels = df$Filt)
cls2<- c("tomato3","grey50","forestgreen")
p2<-ggplot(data=df, aes(x=Filt, y=Counts/1000, group=1)) + 
  geom_line(color="gray28") + geom_point(color="gray28") + 
  theme_classic() +
  #theme(plot.margin = margin(0,0,0,1,"cm"))+
  scale_x_discrete(labels = wrap_format(15)) +
  #labs(title="Number of fusions at each pipeline step", subtitle="aggregated by breakpoint and method", x=NULL,y = "fusions / 1000") +
  labs(title="null", subtitle="combined", x=NULL,y = "fusions / 1000") +
    theme(legend.position="top",
        plot.title = element_text(hjust = 0.5,size=25,color="white"),
        plot.subtitle = element_text(hjust = 0,size=15),
        legend.title = element_blank(), 
        legend.text=element_text(size=12), 
        axis.text=element_text(size=12), 
        axis.title=element_text(size=12),
        axis.text.x = element_text(angle = 45,hjust=1,color=cls2)) +
  geom_text(label=df[,2],nudge_y = 0.7, nudge_x=0.2,size=3) +
  theme(plot.margin=grid::unit(c(0,0,0,0), "mm")) 

# combien plots
p3 <- p1+p2 + plot_layout(ncol=2,widths=c(4.1,1)) 

p4<-p3 + plot_annotation(title="Number of fusions at each pipeline stage",
                         theme = theme(plot.title = element_text(size = 18,hjust=0.5)))


ggsave("OutputPlots/line_plot_combined.pdf",p4,device="pdf")
ggsave("OutputPlots/line_plot_combined.jpg",p4,device="jpeg")


## Supplemntary Figure 2
## Overlap of fusion calls         

methods <- read.csv("InputFiles/upsetTable.csv",sep="\t",header = T,check.names = F)
cols=c("#7570B3","#E7298A", "#E6AB02","#1B9E77","#D95F02","#66A61E","#666666")
pdf(file="OutputPlots/upset_fig.pdf", onefile=FALSE,height=5) 
upset(methods,nsets=7,nintersects = 100,query.legend = "bottom",
      #order.by=c("degree"),
      #order.by=c("freq"),
      order.by = c("degree","freq"),decreasing=c(F,T),
      #order.by = c("freq","degree"),decreasing=c(T,T),
      sets.bar.color=cols,
      queries = list(list(query = intersects,
                          params = list("arriba","chimerascan","starfusion","soapfuse","ericscript","fusioncatcher","fusioninspector"), 
                          color = "indianred", 
                          active = T,
                          query.name="All methods"),
                     list(query = intersects,params = list("arriba","fusioninspector","starfusion","chimerascan","fusioncatcher"), 
                          color = "cadetBlue", 
                          active = T,
                          query.name="Largest consensus")
                         ),
      text.scale = c(1, 1, 0.8, 1, 1, 0.8),
      point.size = 1.5)
      
grid.text("Overlap between fusion calling methods",x = 0.65, y=0.95, gp=gpar(fontsize=13))
dev.off()
#ggplot()
#ggsave("OutputPlots/upset.jpg",device="jpeg")
