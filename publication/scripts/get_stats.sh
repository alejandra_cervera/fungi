
out="out_fungi/"
outFC="${out}/FC"
outS="stats"
mkdir -p $outS

# FusionCaller stats
for method in arriba chimerascan ericscript fusioncatcher soapfuse starfusion
do
  wc -l ${outFC}/*-${method}/final | awk 'BEGIN {print "Called\tKey"}{n=split($2,a,"/"); print $1-1,a[n-1]}' OFS="\t" > ${outS}/called_${method}
done

# Filtered fusion stats
for method in chimerascan fusioncatcher starfusion
do
  wc -l ${out}/Filtered_FC/*-${method}/final | awk 'BEGIN {print "Called\tKey"}{n=split($2,a,"/"); print $1-1,a[n-1]}' OFS="\t" > ${outS}/called_${method}
done

# Methods that were not customed filtered have the same number of fusions in both steps
for method in arriba ericscript soapfuse
do
  sed '1s/Called/Custom/' ${outS}/called_${method} > ${outS}/custom_${method}
done

# FusionAnalyzer stats
outFA="${out}/FA"

# from fusion-analyzer tmpdir we can count fusions at each filtering step
for method in arriba chimerascan ericscript fusioncatcher soapfuse starfusion
do
  wc -l ${outFA}/tmpDir/split_filtered/*${method}.csv | awk 'BEGIN {print "SplitCount\tKey"}{print $1-1,$2}' OFS="\t" | sed "s|${outFA}/tmpDir/split_filtered/||" | sed 's/.csv//' | sed "s/_${method}/-${method}/" > ${outS}/split_${method} 
  wc -l ${outFA}/tmpDir/fusions_filtered/ensembl_filtered/*${method}.csv | awk 'BEGIN {print "AnnotCount\tKey"}{print $1-1,$2}' OFS="\t" | sed "s|${outFA}/tmpDir/fusions_filtered/ensembl_filtered/||" | sed 's/.csv//' | sed "s/_${method}/-${method}/" > ${outS}/ensembl_filtered_${method}
  wc -l ${outFA}/tmpDir/fusions_filtered/db_filtered/*${method}.csv | awk 'BEGIN {print "DBCount\tKey"}{print $1-1,$2}' OFS="\t" | sed "s|${outFA}/tmpDir/fusions_filtered/db_filtered/||" | sed 's/.csv//' | sed "s/_${method}/-${method}/" > ${outS}/db_filtered_${method}
  awk -F "\t" -v m=${method} 'NR>1 && $9~m {print $9}' ${outFA}/scored/final.candidates.csv | sort | uniq -c | awk 'BEGIN {print "ScoredCount\tKey"}{print $1,$2}' OFS="\t" | sed "s/_${method}/-${method}/" > ${outS}/scored_${method}
done

#summary of all counts
echo method called custom split ensembl_filtered db_filtered scored | tr ' ' '\t' > ${outS}/out1.csv
for method in arriba chimerascan ericscript fusioncatcher soapfuse starfusion
do
  a=$( awk 'END {print $1}' ${outS}/called_${method} )
  b=$( awk 'END {print $1}' ${outS}/custom_${method} )
  c=$( awk 'END {print $1}' ${outS}/split_${method} )
  d=$( awk 'END {print $1}' ${outS}/ensembl_filtered_${method} )
  e=$( awk 'END {print $1}' ${outS}/db_filtered_${method} )
# scored counts were obtained from a single file so it does not have the total count as the last line like the others
  f=$( awk '{count=$1+count;} END {print count-1}' ${outS}/scored_${method} )
  echo ${method} $a $b $c $d $e $f | tr ' ' '\t' >> ${outS}/out1.csv
done

head -q -n -1 ${outS}/called* | grep -v Key > ${outS}/summary_called
head -q -n -1 ${outS}/custom* | grep -v Key > ${outS}/summary_custom
head -q -n -1 ${outS}/split* | grep -v Key > ${outS}/summary_split
head -q -n -1 ${outS}/ensembl* | grep -v  Key > ${outS}/summary_ensembl
head -q -n -1 ${outS}/db* | grep -v Key > ${outS}/summary_db
cat ${outS}/scored* | grep -v Key > ${outS}/summary_scored

python make_table.py ${outS} ${outS}/methods_fusion_count_table.csv

tr '-' '\t' < methods_fusion_count_table.csv | sed '1s/\tCalled/Sample\tMethod\tCalled/'  > ../InputFiles/final_methods_fusion_count_table.csv

# make table for second part of the plot
fv_filtered=${out}/Final_Fusion_Genes_OvCa_fungi_FV_PostFilter_2/formatted_filtered_fv.csv
fa_filtered=${out}/Final_Fusion_Genes_OvCa_fungi_FA_PostFilter/filt_3_fusions.csv
cut -f1 $fa_filtered | tail -n +2 | sort -u > ${outS}/fa_sorted.csv
cut -f1 $fv_filtered | tail -n +2 | sort -u > ${outS}/fv_sorted.csv
join ${outS}/fa_sorted.csv ${outS}/fv_sorted.csv > match_fa_fv_filtered.csv
visFilter=$( wc -l match_fa_fv_filtered.csv | cut -d " " -f1 )
#awk -F"\t" 'NR>1 {print $1"&"$2"\tfusioninspector"}' | sort -u > ${outS}/fv_sorted.csv


outFA="${out}/Final_Fusion_Genes_OvCa_fungi_FA_4"

scored=$( cut -f1 ${outFA}/scored/final.candidates.csv | tail -n +2 | sort -u | wc -l )
filt=$( cut -f1 ${out}/Final_Fusion_Genes_OvCa_fungi_FA_PostFilter/filt_3_fusions.csv | tail -n +2 | sort -u | wc -l )
#visFilter=$( grep fusioninspector ${out}/Final_Fusion_Genes_OvCa_fungi_FConsensus/report.csv | cut -f1 | sort -u | wc -l )
echo -e "Scored\tCustomFilter\tMatchFV" > out2.csv
echo -e $scored"\t"$filt"\t"$visFilter >> out2.csv

#esta se creo con una nueva version de filtering.py
in=${out}/Final_Fusion_Genes_OvCa_fungi_FA_PostFilter/filtered_final_candidates.csv
awk -F"\t" 'NR>1 {print $1"_"$33,$13}' OFS="\t" $in | sort -u > ${outS}/fa_filtered_main_cols
awk -F"\t" 'NR>1 {print $1"_"$2,$3}' OFS="\t" $fv_filtered | sort -u > ${outS}/fv_filtered_main_cols
join ${outS}/fa_filtered_main_cols ${outS}/fv_filtered_main_cols | sed 's/ /\t/' | tr ' ' ',' > ${outS}/joined
awk -F"\t" '{f[$1]=$2","f[$1];} END {for (i in f) print i,f[i]}' OFS="\t" ${outS}/joined | awk -F"\t" 'BEGIN {print "Fusion\tarriba\tchimerascan\tericscript\tfusioncatcher\tfusioninspector\tsoapfuse\tstarfusion"} {split($2,a,","); b["arriba"]=0; b["chimerascan"]=0; b["ericscript"]=0; b["fusioncatcher"]=0; b["fusioninspector"]=0; b["soapfuse"]=0; b["starfusion"]=0; for (i in a) {b[a[i]]=1;} print $1,b["arriba"],b["chimerascan"],b["ericscript"],b["fusioncatcher"],b["fusioninspector"],b["soapfuse"],b["starfusion"];}' OFS="\t" > upsetTable.csv

