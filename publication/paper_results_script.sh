#!/bin/bash


########## FusionCaller ##########
##################################

mkdir -p out_fungi
input_list="mySampleKey_SamplePath.csv"

outFC="out_fungi/FC"
mkdir -p ${outFC}

while read key reads mates 
do
  fungi-fusion-caller -c myConfig.txt -r ${reads} -m ${mates} -o ${outFC}/${key}-arriba -t arriba -s ${key} -th 8
  fungi-fusion-caller -c myConfig.txt -r ${reads} -m ${mates} -o ${outFC}/${key}-chimerascan -t chimerascan -s ${key} -th 8
  fungi-fusion-caller -c myConfig.txt -r ${reads} -m ${mates} -o ${outFC}/${key}-ericscript -t ericscript -s ${key} -th 8
  fungi-fusion-caller -c myConfig.txt -r ${reads} -m ${mates} -o ${outFC}/${key}-fusioncatcher -t fusioncatcher -s ${key} -th 8
  fungi-fusion-caller -c myConfig.txt -r ${reads} -m ${mates} -o ${outFC}/${key}-soapfuse -t soapfuse -s ${key} -th 8 -l 102
  fungi-fusion-caller -c myConfig.txt -r ${reads} -m ${mates} -o ${outFC}/${key}-starfusion -t starfusion -s ${key} -th 8
done < ${input_list}

# Output available at https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/Publication/TarBalls/
##################################


########## Filtering #############

### SoapFuse, EricScript and Arriba outputs will be sent directly to FusionAnalyzer while FusionCatcher, ChimeraScan and STAR-Fusion will be prefiltered

out_filt_1="out_fungi/Filtered_FC"
mkdir -p ${out_filt_1}

# Add output paths from SoapFuse, Arriba and Ericscript to a tmpfile
ls -d ${outFC}/*-arriba > tmpFile
ls -d ${outFC}/*-ericscript >> tmpFile
ls -d ${outFC}/*-soapfuse >> tmpFile

# fungi-prep-analyzer-input consumes a list of ouput directories from fusion-caller and preps a csv file with Sample, Tool, File columns
fungi-prep-analyzer-input -i tmpFile -o ${out_filt_1}/fa_input.csv


### Custom filtering steps for Chimerascan, STAR-Fusion, and FusionCatcher

# Steps:
# for each fusion-caller output directory we fetch tool and sample name from .tool and .sample files in the directory
# we remove fusions based on different criteria for each tool
# we append the new fusion file to our list of input files for fusion-analyzer (cols: Sample Tool File)

#Filtering FusionCatcher results and add new results files to ${out_filt_1}/fa_input.csv
for i in $( ls -d ${outFC}/*-fusioncatcher )
do
  key=$( cat $i/.sample )
  tool=$( cat $i/.tool )
  out=${out_filt_1}/${key}_filtered_${tool}.csv
  awk -F"\t" '{if (NR==1) {for (i=1; i<=NF; i++) {if ($i~"Counts_of_common_mapping_reads") {n=i;}} print $0;} else {if ($n<=4) print $0;}}' OFS="\t" ${i}/final > ${out}
  echo -e ${key}"\t"${tool}"\t"${out} >> ${out_filt_1}/fa_input.csv 
done

#Filtering Chimerascan results and add new results files to ${out_filt_1}/fa_input.csv
for i in $( ls ${outFC}/*-chimerascan )
do
  key=$( cat $i/.sample )
  tool=$( cat $i/.tool )
  out=${out_filt_1}/${key}_filtered_${tool}.csv
  awk -F"\t" '{if (NR==1) {for (i=1; i<=NF; i++) {if ($i~"type") {n=i;}} print $0;} else {if ($n !~ "Overlapping_Same") print $0;}}' OFS="\t" ${i}/final > ${out}
  echo -e ${key}"\t"${tool}"\t"${out} >> ${out_filt_1}/fa_input.csv 
done

#Filtering STAR-Fusion results and add new results files to ${out_filt_1}/fa_input.csv
for i in $( ls ${outFC}/*-starfusion )
do
  key=$( cat $i/.sample )
  tool=$( cat $i/.tool )
  out=${out_filt_1}/${key}_filtered_${tool}.csv
  awk -F"\t" '{if (NR==1) {for (i=1; i<=NF; i++) {if ($i~"LargeAnchorSupport") {n=i;}} print $0;} else {if ($n ~ "YES_LDAS") print $0;}}' OFS="\t" ${i}/final > ${out}
  echo -e ${key}"\t"${tool}"\t"${out} >> ${out_filt_1}/fa_input.csv 
done

##Example on how to filter Arriba (this was not used in the publication)
#for i in $( ls ${outFC}/*-arriba )
#do
#  key=$( cat $i/.sample )
#  tool=$( cat $i/.tool )
#  out=${out_filt_1}/${key}_filtered_${tool}.csv
#  awk -F"\t" '{if (NR==1) {for (i=1; i<=NF; i++); if ($i~"confidence") {n=i;} print $0;} else {if ($n !~ "low") print $0;} }' OFS="\t" ${i}/final > ${out}
#  echo -e ${key}"\t"${tool}"\t"${out} >> ${out_filt_1}/fa_input.csv 
#done

####################################


########## FusionAnalyzer ##########
####################################

outFA="out_fungi/FA"

# Both annotate and score the fusions and filter out uninteresting fusions based on ensembl and other databases annotations
# same config file as fungi-fusion-caller step
# -input is a file with Sample, Tool and File cols where file is the filtered or unfiltered fusion calls 
# --filter-ensembl removes uninteresting fusions based on homology or lack of function 
# --filter-db removes fusions that have been reported in non-cancer databases

fungi-fusion-analyzer annotate scoring -c myConfig.txt -i ${out_filt_1}/fa_input.csv -o ${outFA} --tissue EPI --clearDB --filter-ensembl invalid_gene_id,invalid_gene_name,mismatch_with_ensembl,same_gene,homologous,no_GO_term --filter-db bodymap2,cacg,conjoining,cta,ctb,ctc,ctd,duplicates,overlapping,fragments,gtex,metazoa,mirna,mt,non_cancer_tissues,non_tumor_cells,pair_pseudo_genes,paralogs,rp11,rp,rrna,similar_reads,similar_symbols,yrna,pairs_pseudogenes,no_protein,healthy,conjoing,bodymap,1000genomes 

# Output available at https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/Publication/final.candidates.csv
####################################


########## Filtering ###############


out_filt_2="out_fungi/Filtered_FA"
mkdir -p ${out_filt_2}

# remove fusions only called by ericscript -> filt_1_fusions.csv
# remove fusions that were annotated in healthy tissue but not in cancer (ex. banned,hpa is removed while banned,18cancers is kept) -> filt_2_fusions.csv
# remove fusions with Pegasus_score < 0.25 and Oncofuse_score < 0.6 -> filt_3_fusions.csv
# filtering.py script is also included in the bitbucket publication folder
python filtering.py ${outFA}/scored/final.candidates.csv ${out_filt_2}


# Output available at https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/Publication/filtered_final_candidates.csv
######################################


########## FusionVisualyzer ##########
######################################

outFV="out_fung/FV"
mkdir -p ${outFV}

# Supervised calling of the filtered fusions list with FusionInspector
# same config file as in fungi-fusion-calller and fungi-fusion-analyzer
# same reads and mates used in fungi-fusion-caller
while read key reads mates 
do
  fungi-fusion-visualizer -c myConfig.txt -i ${out_filt_2}/filt_3_fusions.csv -r $reads -m $mates --sample ${key} -o ${outFV}/${key} -th 8
done < ${input_list}


######################################


########## Filtering #################

out_filt_3="out_fungi/Filtered_FV"
mkdir -p ${out_filt_3}

# combine all spearate fusion inspector files into a single file that includes sample name after the fusion name column
grep "" ${outFV}/*/*tsv.annotated | sed 's/:/\t/' | awk -F"\t" 'NR==1 || $2 != "#FusionName" {n=split($1,a,"/"); split(a[n],b,"."); $1=b[1]; if (NR==1) {$1="Sample";} tmp1=$1; $1=$2; $2=tmp1; print $0}' OFS="\t" > ${out_filt_3}/combined_fv.csv

# filtering_fv.py outputs both the filtered fusions in the original format as well as a ready format for FusionConsensus
# keep fusions with JunctionReadCount >= 3
# keep fusions with FFPM >=0.1 (rounded)
python filtering_fv.py ${out_filt_3}/combined_fv.csv $out_filt_3
out=${out_filt_3}/formatted_filtered_fv.csv


# Output available at https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/Publication/combined_fv.csv
# Output available at https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/Publication/formatted_filtered_fv.csv
######################################


########## FusionConsensus ###########
######################################

outFConsensus="out_fungi/FConsensus"
mkdir -p ${outConsensus}

# Find the consensus breakpoint for each fusion
# --input is the fusions in general input format; here we are using FusionInspector outputs already formatted in previous step
# --fusion_analyzer has the original breakpoint information for each method; here we use a filtered file created with cusomt filtering step (filtering.py)
fungi-fusion-consensus --input ${out} --fusion_analyzer ${out_filt_2}/filtered_final_candidates.csv -o ${outFConsensus} 

# Output available at https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/Publication/bp_consensus_report.csv
# Output available at https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/Publication/combined_fusions_report.csv
######################################


########## Filtering #################

# Remove fusions not detected by FusionInspector
awk -F"\t" 'NR==1 || /fusioninspector/' ${outFConsensus}/combined_fusions_report.csv > out_fungi/final_filtered_fusion_report.csv

# Output available at https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/Publication/final_filtered_fusion_report.csv
