#!/bin/bash

#requires reads.fq.gz mates.fq.gz that can be downloaded with download.sh in resources folder

reads=../../resources/reads.fq.gz
mates=../../resources/mates.fq.gz
tool="arriba"

rm -rf out_arriba
bash ../../bin/fungi-fusion-caller \
    -c ../test-conf.txt \
    -r $reads \
    -m $mates \
    -o out_arriba \
    -t $tool \
    -s sample1 \
    -th 8 \
    debug


