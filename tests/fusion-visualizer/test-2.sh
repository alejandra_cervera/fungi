#!/bin/bash

# tests fusion reconstruction WITHOUT breakpoints which uses FusionInspector
# requires reads, mates, and fusion_list from resources folder that can be downloaded with download.sh

reads=../../resources/reads.fq.gz
mates=../../resources/mates.fq.gz
FUSION_LIST=../../resources/fv_input.csv

FUSION_LIST=../../resources/fv_input.csv
reads=../../resources/reads.fq.gz
mates=../../resources/mates.fq.gz
threads=2

bash ../../bin/fungi-fusion-visualizer \
    -c ../test-conf.txt \
    -o output-2 \
    -i "$FUSION_LIST" \
    -r "$reads" \
    -m "$mates" \
    --threads $threads \
    --sample sample1 \
    debug
