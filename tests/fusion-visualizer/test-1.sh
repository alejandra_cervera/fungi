#!/bin/bash

# tests fusion reconstruction with breakpoints (--use-bp parameter)
# requires reads, mates, and fusion_list from resources folder that can be downloaded with download.sh

reads=../../resources/reads.fq.gz
mates=../../resources/mates.fq.gz
FUSION_LIST=../../resources/fv_input.csv

gene_id5p_coln="gene5p_id"
gene_id3p_coln="gene3p_id"
bps="bp_coordinates"
threads=2
AS=0.95

bash ../../bin/fungi-fusion-visualizer \
    -c ../test-conf.txt \
    -o output-1 \
    -i "$FUSION_LIST" \
    -r "$reads" \
    -m "$mates" \
    --use-bp \
      --gene5p-id $gene_id5p_coln \
      --gene3p-id $gene_id3p_coln \
      --bp-coordinates $bps \
      --as $AS \
      --ref-genome $fasta \
    --threads $threads \
    --sample sample1 \
    debug
