#!/bin/bash


echo "Going to delete all test case outputs in 10 seconds
(Ctrl-C to cancel, or press enter to do it now)"

for i in {1..10}; do
  printf "\r%d.." $(( 10 - i ))
  read -t 1 -n 1 key
  if [[ $? -eq 0 ]]; then
    break
  fi
done

echo
set -e
for p in fusion-analyzer/generic_fusions.csv \
         fusion-analyzer/test-output/ \
         fusion-caller/out_arriba/ \
         fusion-consensus/output-1 \
         fusion-consensus/output-2 \
         fusion-visualizer/output-1/ \
         fusion-visualizer/output-2/ \
; do
    rm -rfv "$p"
done
