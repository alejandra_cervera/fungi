#!/bin/bash

# tests only import
# requires fa_input.csv that can be downloaded with download.sh in resources folder

# Link the generic fusions here from resources:
if [[ ! -e generic_fusions.csv ]]; then
    ln -s ../../resources/generic_fusions.csv
fi


bash ../../bin/fungi-fusion-analyzer \
  -o test-output \
  -c ../test-conf.txt \
  -i ../../resources/fa_input.csv \
  debug


