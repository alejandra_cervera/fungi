#!/bin/bash

# tests only annotate
# output folder has to be the same as test1; no input given

bash ../../bin/fungi-fusion-analyzer \
  -o test-output \
  -c ../test-conf.txt \
  annotate \
  debug


