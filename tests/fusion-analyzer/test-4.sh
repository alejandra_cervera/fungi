#!/bin/bash

# tests annotate & scoring
# requires fa_input.csv that can be downloaded with download.sh in resources folder

# Link the generic fusions here from resources:
if [[ ! -e generic_fusions.csv ]]; then
    ln -s ../../resources/generic_fusions.csv
fi


bash ../../bin/fungi-fusion-analyzer \
  -o test-output \
  -i ../../resources/fa_input.csv \
  -c ../test-conf.txt \
  annotate \
  scoring \
  --tissue AVG \
  --clearDB \
  --filter-ensembl invalid \
  debug

