#!/bin/bash

# tests only scoring, requires annotate step to have been run beforehand and the same output folder specified

bash ../../bin/fungi-fusion-analyzer \
  -o test-output \
  -c ../test-conf.txt \
  scoring \
  --tissue AVG \
  --clearDB \
  debug


