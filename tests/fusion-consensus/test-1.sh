#!/bin/bash

# tests fusion consensus
# requires fcs_input.csv, fcs_fusion_analyzer.csv, fcs_fusion_list.csv from resources folder that can be downloaded with download.sh

fcs_input=../../resources/fcs_input.csv
fcs_fusion_analyzer=../../resources/fcs_fusion_analyzer.csv
fcs_fusion_list=../../resources/fcs_fusion_list.csv

bash ../../bin/fungi-fusion-consensus \
    -o "output-1" \
    --input "$fcs_input" \
    --fusion_analyzer "$fcs_fusion_analyzer" \
    --fusion_list "$fcs_fusion_list" \
    debug

