Test commands
=============

For the tests to run, you need to create a
**test-conf.txt** file in this folder, with the tool paths set up correctly
in your system.

A template can be created with:
`../bin/fungi-prepare-config -c test-conf.txt`

For reference, see the configuration that was used to test the scripts:
[Example configuration](example-conf.txt)

All test commands are run with debug mode on, so it's easier
to pinpoint at which command the run fails.

