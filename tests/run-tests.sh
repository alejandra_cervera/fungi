#!/bin/bash
set -e
set -x

cd $( dirname $( readlink -f $0 ) )
for dir in fusion-caller \
           fusion-analyzer \
           fusion-visualizer \
           fusion-consensus; do
    pushd "$dir"
    for script in test*sh; do
        echo y | bash "$script"
    done
    popd &>/dev/null
done


