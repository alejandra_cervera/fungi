#!/usr/bin/Rscript

#library(biomaRt)
options(width=200)

args <- commandArgs(TRUE)
var1 <- args[1] #FusionMatcher output "extensive"
OUT_DIR <- args[2]  #output directory
resource_path<-args[3]   # pre downloaded data from biomart
id_tool<-args[4]

## Set the output dir of the pipeline
fusions_table <- read.table(var1,header=TRUE,sep="\t",stringsAsFactors=F)
if (nrow(fusions_table)==0) {
  print("No fusions left")
  quit(status=0)
}


## TODO: Try/catch  -> download / error
#~ print("#RETRIEVING DATA FROM DATABASE")
load( paste(resource_path,"homolog_table.Rdata",sep="/") )
load( paste(resource_path,"GO.Rdata",sep="/") )
load( paste(resource_path,"geneid.Rdata",sep="/") )

names(homolog_table)[names(homolog_table) == "gene_name_of_homolog" ] <- "homolog_name_gene1"
names(homolog_table)[names(homolog_table) == "gene_name_of_id" ] <- "gene1_name"

fusions <-data.frame(
    cbind(
        fusions_table,
        data.frame(
            ensembl_gene_id1=NA,
            start_position1=NA,
            end_position1=NA,
            ensembl_gene_id2=NA,
            start_position2=NA,
            end_position2=NA,
            ensembl_annot=NA
        )
    )
)

for(j in 1:length(fusions$gene_name1)){

    print(paste(fusions[j,"gene_name1"],fusions[j,"gene_name2"]," fusion",j,"of ",length(fusions[[1]]),"for ",fusions[j,"sample"],fusions[j,"method"],sep=" "))
    fusions[j,"ensembl_annot"]=""

    #if no id given gene1, add one
    if (is.na(fusions$gene_id1[j]) || length(fusions$gene_id1[j]) < 2) {
        id=gene_name_id$ensembl_gene_id[which(gene_name_id$external_gene_name==fusions$gene_name1[j],)]
        #if id is empty then the gene_name was not found in ensembl
        if (rlang::is_empty(id)) {
            fusions[j,"ensembl_annot"]="invalid_gene_name"
            next
        }
        #add id
        fusions$gene_id1[j]=id
    }
    else {
        #if id given check that is valid
        external_name=gene_name_id[(gene_name_id$ensembl_gene_id==fusions$gene_id1[j]),"external_gene_name"]
        #if external_name is emtpy then gene_id was not found in ensembl
        if (rlang::is_empty(external_name)) {
            fusions[j,"ensembl_annot"]="invalid_gene_id"
            next
        }
        #id is valid but gene is not the same, change name but report it
        if (fusions$gene_name1[j]!=external_name) {
            fusions$gene_name1[j]=external_name
            fusions[j,"ensembl_annot"]=paste(fusions[j,"ensembl_annot"],paste("name_change",fusions$gene_name1[j],external_name,sep="_"),collapse=",")
        }
    }

    #if no id given gene2, add one
    if (is.na(fusions$gene_id2[j]) || length(fusions$gene_id2[j]) < 2 ) {
        id=gene_name_id$ensembl_gene_id[which(gene_name_id$external_gene_name==fusions$gene_name2[j],)]
        #if id is empty then the gene_name was not found in ensembl
        if (rlang::is_empty(id)) {
            fusions[j,"ensembl_annot"]="invalid_gene_name"
            next
        }
        #add id
        fusions$gene_id2[j]=id
    }
    else {
        #if id given check that is valid
        external_name=gene_name_id[(gene_name_id$ensembl_gene_id==fusions$gene_id2[j]),"external_gene_name"]
        #if external_name is emtpy then gene_id was not found in ensembl
        if (rlang::is_empty(external_name)) {
            fusions[j,"ensembl_annot"]="invalid_gene_id"
            next
        }
        #id is valid but gene is not the same, change name but report it
        if (fusions$gene_name2[j]!=external_name) {
            fusions$gene_name2[j]=external_name
            fusions[j,"ensembl_annot"]=paste(fusions[j,"ensembl_annot"],paste("name_change",fusions$gene_name2[j],external_name,sep="_"),collapse=",")
        }
    }

    g1_id=gene_name_id[ ( gene_name_id$ensembl_gene_id == fusions$gene_id1[j] & # id matches
                           gene_name_id$chromosome_name == fusions$chr1[j] &     # chromosome matches
                           gene_name_id$start_position <= fusions$bp1[j] &       # starts before breakpoint
                           gene_name_id$end_position >= fusions$bp1[j]           # ends after breakpoint
                         ),]

    g2_id=gene_name_id[ ( gene_name_id$ensembl_gene_id == fusions$gene_id2[j] & # id matches
                           gene_name_id$chromosome_name == fusions$chr2[j] &     # chromosome matches
                           gene_name_id$start_position <= fusions$bp2[j] &       # starts before breakpoint
                           gene_name_id$end_position >= fusions$bp2[j]           # ends after breakpoint
                         ),]


    fusions[j,"ensembl_gene_id1"]=g1_id$ensembl_gene_id[1]
    fusions[j,"start_position1"]=g1_id$start_position[1]
    fusions[j,"end_position1"]=g1_id$end_position[1]
    fusions[j,"ensembl_gene_id2"]=g2_id$ensembl_gene_id[1]
    fusions[j,"start_position2"]=g2_id$start_position[1]
    fusions[j,"end_position2"]=g2_id$end_position[1]
#    fusions[j,"ensembl_annot"]=paste(fusions[j,"ensembl_annot"],"",sep="")

    #print(fusions)

    if (nrow(g1_id)==0 | nrow(g2_id)==0) {
        fusions[j,"ensembl_annot"]=paste(fusions[j,"ensembl_annot"],"mismatch_with_ensembl",collapse=",")
        next
    }

    my_homologs_table.homologs_name <- homolog_table$homolog_name_gene1[homolog_table$gene1_name %in% fusions[j,1]]

    #test if gene1 and gene2 are homologous
    if (fusions[j,2] %in% my_homologs_table.homologs_name ) {
        # at least one found, skip to next iteration
        fusions[j,"ensembl_annot"]=paste(fusions[j,"ensembl_annot"],"homologous",sep=",")
        #next
    }
    #test if gene1 and gene2 are the same
    if (fusions[j,1] == fusions[j,2]) {
        # at least one found, skip to next iteration
        fusions[j,"ensembl_annot"]=paste(fusions[j,"ensembl_annot"],"same_gene",sep=",")
        #next
    }

    #test if neither gene has a known function
    go1_res<-GO$external_gene_name %in% fusions[j,1]
    go2_res<-GO$external_gene_name %in% fusions[j,2]
    if ( !any(go1_res) && !any(go2_res) ) {
        fusions[j,"ensembl_annot"]=paste(fusions[j,"ensembl_annot"],"no_GO_term",sep=",")
        #next
    }
    fusions$ensembl_annot[is.na(fusions$ensembl_annot)] <- ""
}

#~ print("Input fusions")
#~ print(fusions[[1]])
#~ print("Included fusions")
#~ print(fusions_filtered_in)
#~ print("Excluded fusions")
#~ print(cbind(fusions_filtered_out,note=reason))

write.table(fusions,file=paste(OUT_DIR,paste(id_tool,"csv",sep="."),sep="/"),row.names=F, sep="\t", quote=FALSE)

