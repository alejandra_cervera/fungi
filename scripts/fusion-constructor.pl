#!/usr/bin/perl


my ($gene_id5p_coln,$gene_id3p_coln,$bps,$FUSION_LIST,$ANNOTATION_FILE,$fasta,$reads1,$reads2,$OUT_DIR,$threads,$AS,$INPUT_FORMAT,$GENOME_IDX1,$GENOME_IDX2) = @ARGV;

#Bio::Tools::Run::Ensembl->registry_setup(-verbose => 0, -db_version => $REF_GENOME);
#$adp = 'Bio::EnsEMBL::DBSQL::DBAdaptor';
#my $slice_adaptor = Bio::Tools::Run::Ensembl->get_adaptor('human','Slice');


# Reading Fusion List file
open CANDIDATE_FUSION_H, "<", $FUSION_LIST or die $!;
$line = <CANDIDATE_FUSION_H>;  # keep header line
chop($line);
@tmp_line = split("\t",$line);
$i=0;
%col_num=map {$_=>$i++} @tmp_line;
while ($line= <CANDIDATE_FUSION_H>){
  chop($line);
  @tmp_line = ();
  push(@tmp_line,split("\t", $line));
  # gene id for the first gene in the fusion
  if(exists($col_num{$gene_id5p_coln})){
    push(@gene_id5p, $tmp_line[$col_num{$gene_id5p_coln}]);
    print "gene_id5p: $tmp_line[$col_num{$gene_id5p_coln}]\n";
  }
  else{
   die "$gene_id5p_coln column not found in fusion list file.\n;"
  }
  # gene id for the second gene in the fusion
  if(exists($col_num{$gene_id3p_coln})){
    push(@gene_id3p, $tmp_line[$col_num{$gene_id3p_coln}]);
    print "gene_id3p: $tmp_line[$col_num{$gene_id3p_coln}] \n";
  }
  else{
   die "$gene_id3p_coln column not found in fusion list file.\n;"
  }
  # we split the bp_coordinates column, each gene coordinates is separated by ">"
  if(exists($col_num{$bps})){
    ($coord5p,$coord3p) = split(">",$tmp_line[$col_num{$bps}]);
    print "bps: $coord5p $coord3p \n";
  }
  else{
   die "$bps column not found in fusion list file.\n;"
  }
  # the chr from the breakpoing coords is separate by ":"
  ($myChr5p,$bp5p)= split(":",$coord5p);
  ($myChr3p,$bp3p)= split(":",$coord3p);
  push(@gene_chr5p, $myChr5p);
  push(@gene_chr3p, $myChr3p);
  push(@gene_bp5p, $bp5p);
  push(@gene_bp3p, $bp3p);
  #print "$line\n";
  #print "original: $gene_id5p_coln $gene_id3p_coln $bps $tmp_line[$col_num{$gene_id5p_coln}] $tmp_line[$col_num{$gene_id3p_coln}] \n";
  #print "candidates: $tmp_line[$col_num{$gene_id5p_coln}] $tmp_line[$col_num{$gene_id3p_coln}] $myChr5p $myChr3p $bp5p $bp3p \n"
  #print "candidates: $gene_id5p[$i] $gene_id3p[$i] $gene_bp5p[$i] $gene_bp3p[$i] $gene_chr5p[$i] $gene_chr3p[$i]   \n";
  #$i=$i+1;
}
close CANDIDATE_FUSION_H;

# Reading annotation file
open ANNOTATION_FILE_H, "<", $ANNOTATION_FILE or die $!;
@annotation_file = <ANNOTATION_FILE_H>;
close ANNOTATION_FILE_H;

#print "$OUT_DIR\n";
print "\nCreating virtual references...\n";
open VIRTUAL_REF1_H, ">", "$OUT_DIR/tmp_virtual_ref1.fa" or die $!;
open VIRTUAL_REF2_H, ">", "$OUT_DIR/tmp_virtual_ref2.fa" or die $!;
open GFF1, ">", "$OUT_DIR/annotation1.gff3" or die $!;
open GFF2, ">", "$OUT_DIR/annotation2.gff3" or die $!;
open GTF1, ">", "$OUT_DIR/annotation1.gtf" or die $!;
open GTF2, ">", "$OUT_DIR/annotation2.gtf" or die $!;
%fusion_pair_is_present; # Hash table to check if the fusion pair is already present
for ($i = 0; $i < @gene_id5p; $i++) {
  #print "gene_id5p  $gene_id5p[$i] $gene_bp5p[$i]\n";
  #print "gene_id3p  $gene_id3p[$i] $gene_bp3p[$i]\n";
  foreach $line (@annotation_file){
    @tmp_line=split("\t",$line);
    if($tmp_line[2] eq "gene"){
      ($tmp_id) = $line =~(/gene_id "(\S+)"/);
      @annot_id=split('\.',$tmp_id);
      #print "$annot_id[0] $tmp_id $gene_id5p[$i] \n";
      #exit;
      if($gene_id5p[$i] eq $annot_id[0]){
        if($tmp_line[0] eq $gene_chr5p[$i]){
          ($gene_name5p) = $line =~(/gene_name "(\S+)"/);     #grep gene_name
          $chr5p = $tmp_line[0];
          $start5p = $tmp_line[3];
          $end5p = $tmp_line[4];
          $strand5p = $tmp_line[6];
          #print "tmp_line: $tmp_line[0] $tmp_line[1] $tmp_line[2] $tmp_line[3]\n";
          #print "en if: $gene_chr5p[$i] $gene_bp5p[$i]\n";
          #print "if 5p: $gene_id5p[$i] $gene_name5p $chr5p $start5p $end5p $strand5p \n";
        }
      }
      if($gene_id3p[$i] eq $annot_id[0]){
        if($tmp_line[0] eq $gene_chr3p[$i]){
          ($gene_name3p) = $line =~(/gene_name "(\S+)"/);     #grep gene_name
          $chr3p = $tmp_line[0];
          $start3p = $tmp_line[3];
          $end3p = $tmp_line[4];
          $strand3p = $tmp_line[6];
          #print "en if: $gene_chr3p[$i]\n";
          #print "if 3p: $gene_id3p[$i] $gene_name3p $chr3p $start3p $end3p $strand3p \n";
        }
      }
    }
  }
  if($strand5p eq "+"){
    $strand1 = "true";
  }
  else{
    $strand1= "false";
  }
  if($strand3p eq "+"){
    $strand2 = "true";
  }
  else{
    $strand2= "false";
  }
  #print "\nfor loop: $gene_id5p[$i] $gene_id3p[$i] $chr5p $start5p $gene_bp5p[$i] $end5p $strand5p $chr3p $start3p $gene_bp3p[$i] $end3p $strand3p\n";
  # Creating virtual references
  if(!defined $fusion_pair_is_present{"$chr5p_$start5p-$end5p|$chr3p_$start3p-$end3p"}){
    $fusion_pair_is_present{"$chr5p_$start5p-$end5p|$chr3p_$start3p-$end3p"} = 1;
    if($gene_id5p[$i] ne $gene_id3p[$i]){  # Intergenic fusion
      print VIRTUAL_REF1_H "\>$gene_id5p[$i]|$gene_id3p[$i]\n";
      $seq5p = getGenomeSeq($chr5p,$start5p,$end5p,$strand1,$fasta);
      #$seq5p = getGenomeSeq($slice_adaptor,$chr5p,$start5p,$end5p,$strand1);
      $seq5p =~ tr/\n//d;
      print VIRTUAL_REF1_H $seq5p;

      #$seq3p = getGenomeSeq($slice_adaptor,$chr3p,$start3p,$end3p,$strand2);
      $seq3p = getGenomeSeq($chr3p,$start3p,$end3p,$strand2,$fasta);
      $seq3p =~ tr/\n//d;
      print VIRTUAL_REF1_H $seq3p."\n";

      # Creating GFF1 & GTF1 annotation file
      $gene5p_length = $end5p-$start5p;
      $gene3p_length = $end3p-$start3p;
      @greplines = grep(/$gene_id5p[$i]/,@annotation_file);
      foreach $line (@greplines){
        @tmp_line = split("\t",$line);
        if($tmp_line[2] eq "gene"){
          $attributes = $tmp_line[8];
          chop($attributes);
          $attributes = $attributes." color=#FF4500;\n";

          print GFF1 "$gene_id5p[$i]|$gene_id3p[$i]\t$tmp_line[1]\t$tmp_line[2]\t1\t".($gene5p_length+1)."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$attributes";
          print GTF1 "$gene_id5p[$i]|$gene_id3p[$i]\t$tmp_line[1]\t$tmp_line[2]\t1\t".($gene5p_length+1)."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$attributes";
        }
        else{
          if($strand1 eq "true"){
            print GTF1 "$gene_id5p[$i]|$gene_id3p[$i]\t$tmp_line[1]\t$tmp_line[2]\t".($tmp_line[3]-$start5p+1)."\t".($tmp_line[4]-$start5p+1)."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$tmp_line[8]";
          }
          else{
            print GTF1 "$gene_id5p[$i]|$gene_id3p[$i]\t$tmp_line[1]\t$tmp_line[2]\t".($gene5p_length+1-($tmp_line[4]-$start5p))."\t".($gene5p_length+1-($tmp_line[3]-$start5p))."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$tmp_line[8]";
          }
        }
      }
      @greplines = grep(/$gene_id3p[$i]/,@annotation_file);
      foreach $line (@greplines){
        @tmp_line = split("\t",$line);
        if($tmp_line[2] eq "gene"){
          $attributes = $tmp_line[8];
          chop($attributes);
          $attributes = $attributes." color=#20B2AA;\n";

          print GFF1 "$gene_id5p[$i]|$gene_id3p[$i]\t$tmp_line[1]\t$tmp_line[2]\t".($gene5p_length+2)."\t".($gene5p_length+2+$gene3p_length)."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$attributes";
          print GTF1 "$gene_id5p[$i]|$gene_id3p[$i]\t$tmp_line[1]\t$tmp_line[2]\t".($gene5p_length+2)."\t".($gene5p_length+2+$gene3p_length)."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$attributes";
        }
        else{
          if($strand2 eq "true"){
            print GTF1 "$gene_id5p[$i]|$gene_id3p[$i]\t$tmp_line[1]\t$tmp_line[2]\t".($gene5p_length+2+$tmp_line[3]-$start3p)."\t".($gene5p_length+2+$tmp_line[4]-$start3p)."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$tmp_line[8]";
          }
          else{
            print GTF1 "$gene_id5p[$i]|$gene_id3p[$i]\t$tmp_line[1]\t$tmp_line[2]\t".($gene3p_length+$gene5p_length+2-($tmp_line[4]-$start3p))."\t".($gene3p_length+$gene5p_length+2-($tmp_line[3]-$start3p))."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$tmp_line[8]";
          }
        }
      }
    }
    else{#Intragenic fusion
      print VIRTUAL_REF1_H "\>$gene_id5p[$i]|$gene_id3p[$i]\n";
      $seq5p = getGenomeSeq($chr5p,$start5p,$end5p,$strand1,$fasta);
      #$seq5p = getGenomeSeq($slice_adaptor,$chr5p,$start5p,$end5p,$strand1);
      $seq5p =~ tr/\n//d;
      print VIRTUAL_REF1_H $seq5p."\n";

      # Creating GFF1 & GTF1 annotation file
      $gene5p_length = $end5p-$start5p;
      @greplines = grep(/$gene_id5p[$i]/,@annotation_file);
      foreach $line (@greplines){
        @tmp_line = split("\t",$line);
        if($tmp_line[2] eq "gene"){
          $attributes = $tmp_line[8];
          chop($attributes);
          $attributes = $attributes." color=#FF4500;\n";

          print GFF1 "$gene_id5p[$i]|$gene_id3p[$i]\t$tmp_line[1]\t$tmp_line[2]\t1\t".($gene5p_length+1)."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$attributes";
          print GTF1 "$gene_id5p[$i]|$gene_id3p[$i]\t$tmp_line[1]\t$tmp_line[2]\t1\t".($gene5p_length+1)."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$attributes";
        }
        else{  
          if($strand1 eq "true"){
            print GTF1 "$gene_id5p[$i]|$gene_id3p[$i]\t$tmp_line[1]\t$tmp_line[2]\t".($tmp_line[3]-$start5p+1)."\t".($tmp_line[4]-$start5p+1)."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$tmp_line[8]";
          }
          else{
            print GTF1 "$gene_id5p[$i]|$gene_id3p[$i]\t$tmp_line[1]\t$tmp_line[2]\t".($gene5p_length+1-($tmp_line[4]-$start5p))."\t".($gene5p_length+1-($tmp_line[3]-$start5p))."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$tmp_line[8]";
          }
        } 
      }
    }
  }

  if($strand1 eq "true" && $strand2 eq "true"){
    $gene5p_length = $gene_bp5p[$i]-$start5p;
    $gene3p_length = $end3p-$gene_bp3p[$i];

    print VIRTUAL_REF2_H "\>$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\n";
    $seq5p = getGenomeSeq($chr5p,$start5p,$gene_bp5p[$i],"true",$fasta);
    #$seq5p = getGenomeSeq($slice_adaptor,$chr5p,$start5p,$gene_bp5p[$i],"true"); original
    $seq5p =~ s/\n//g;
    print VIRTUAL_REF2_H $seq5p;
     
    $seq3p = getGenomeSeq($chr3p,$gene_bp3p[$i],$end3p,"true",$fasta);
    #$seq3p=getGenomeSeq($slice_adaptor,$chr3p,$gene_bp3p[$i],$end3p,"true");
    $seq3p =~ s/\n//g;
    print VIRTUAL_REF2_H $seq3p."\n";
  }elsif($strand1 eq "false" && $strand2 eq "false"){
    $gene5p_length = $end5p-$gene_bp5p[$i];
    $gene3p_length = $gene_bp3p[$i]-$start3p;

    print VIRTUAL_REF2_H "\>$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\n";
    $seq5p = getGenomeSeq($chr5p,$gene_bp5p[$i],$end5p,"false",$fasta);
    #$seq5p = getGenomeSeq($slice_adaptor,$chr5p,$gene_bp5p[$i],$end5p,"false");
    $seq5p =~ s/\n//g;
    print VIRTUAL_REF2_H $seq5p;

    $seq3p = getGenomeSeq($chr3p,$start3p,$gene_bp3p[$i],"false",$fasta);
    #$seq3p=getGenomeSeq($slice_adaptor,$chr3p,$start3p,$gene_bp3p[$i],"false");
    $seq3p =~ s/\n//g;
    print VIRTUAL_REF2_H $seq3p."\n";
  }elsif($strand1 eq "true" && $strand2 eq "false"){
    $gene5p_length = $gene_bp5p[$i]-$start5p;
    $gene3p_length = $gene_bp3p[$i]-$start3p;

    print VIRTUAL_REF2_H "\>$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\n";
    $seq5p = getGenomeSeq($chr5p,$start5p,$gene_bp5p[$i],"true",$fasta);
#    $seq5p = getGenomeSeq($slice_adaptor,$chr5p,$start5p,$gene_bp5p[$i],"true");
    $seq5p =~ s/\n//g;
    print VIRTUAL_REF2_H $seq5p;

    $seq3p = getGenomeSeq($chr3p,$start3p,$gene_bp3p[$i],"false",$fasta);
    #$seq3p= getGenomeSeq($slice_adaptor,$chr3p,$start3p,$gene_bp3p[$i],"false");
    $seq3p =~ s/\n//g;
    print VIRTUAL_REF2_H $seq3p."\n";
  }elsif($strand1 eq "false" && $strand2 eq "true"){
    $gene5p_length = $end5p-$gene_bp5p[$i];
    $gene3p_length = $end3p-$gene_bp3p[$i];

    print VIRTUAL_REF2_H "\>$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\n";
 
    $seq5p = getGenomeSeq($chr5p,$gene_bp5p[$i],$end5p,"false",$fasta);
#    $seq5p = getGenomeSeq($slice_adaptor,$chr5p,$gene_bp5p[$i],$end5p,"false");
    $seq5p =~ s/\n//g;
    print VIRTUAL_REF2_H $seq5p;

    $seq3p = getGenomeSeq($chr3p,$gene_bp3p[$i],$end3p,"true",$fasta);
#    $seq3p= getGenomeSeq($slice_adaptor,$chr3p,$gene_bp3p[$i],$end3p,"true");
    $seq3p =~ s/\n//g;
    print VIRTUAL_REF2_H $seq3p."\n";
  }


  # Creating GFF2 & GTF2 annotation files
  @greplines = grep(/$gene_id5p[$i]/,@annotation_file);
  foreach $line (@greplines){
    @tmp_line = split("\t",$line);
    if($tmp_line[2] eq "gene"){
      $attributes = $tmp_line[8];
      chop($attributes);
      $attributes = $attributes." color=#FF4500;\n";
      print GFF2 "$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\t$tmp_line[1]\t$tmp_line[2]\t1\t".($gene5p_length+1)."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$attributes";
      print GTF2 "$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\t$tmp_line[1]\t$tmp_line[2]\t1\t".($gene5p_length+1)."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$attributes";
    }else{
      if($strand1 eq "true"){
        if($tmp_line[3]<=$gene_bp5p[$i] && $tmp_line[4]<=$gene_bp5p[$i]){
          print GTF2 "$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\t$tmp_line[1]\t$tmp_line[2]\t".($tmp_line[3]-$start5p+1)."\t".($tmp_line[4]-$start5p+1)."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$tmp_line[8]";
          if($tmp_line[2] eq "transcript"){
            $attributes= $tmp_line[8];
            chop($attributes);
            print GTF2 "$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\t$tmp_line[1]\texon\t".($tmp_line[3]-$start5p+1)."\t".($tmp_line[3]-$start5p+1)."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$attributes exon_number \"0\";\n";
            print GTF2 "$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\t$tmp_line[1]\texon\t".($tmp_line[4]-$start5p+1)."\t".($tmp_line[4]-$start5p+1)."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$attributes exon_number \"0\";\n";
          }
        }elsif($tmp_line[3]<=$gene_bp5p[$i] && $tmp_line[4]>=$gene_bp5p[$i]){
          print GTF2 "$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\t$tmp_line[1]\t$tmp_line[2]\t".($tmp_line[3]-$start5p+1)."\t".($gene5p_length+1)."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$tmp_line[8]";
          if($tmp_line[2] eq "transcript"){
            $attributes= $tmp_line[8];
            chop($attributes);
            print GTF2 "$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\t$tmp_line[1]\texon\t".($tmp_line[3]-$start5p+1)."\t".($tmp_line[3]-$start5p+1)."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$attributes exon_number \"0\";\n";
            print GTF2 "$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\t$tmp_line[1]\texon\t".($gene5p_length+1)."\t".($gene5p_length+1)."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$attributes exon_number \"0\";\n";
                                        }
        }
      }else{
        if($tmp_line[3]>=$gene_bp5p[$i] && $tmp_line[4]>=$gene_bp5p[$i]){
          print GTF2 "$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\t$tmp_line[1]\t$tmp_line[2]\t".($gene5p_length+1-(($tmp_line[4]-$gene_bp5p[$i]+1)-1))."\t".($gene5p_length+1-($tmp_line[3]-$gene_bp5p[$i]))."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$tmp_line[8]";
          if($tmp_line[2] eq "transcript"){
            $attributes= $tmp_line[8];
            chop($attributes);
            print GTF2 "$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\t$tmp_line[1]\texon\t".($gene5p_length+1-(($tmp_line[4]-$gene_bp5p[$i]+1)-1))."\t".($gene5p_length+1-(($tmp_line[4]-$gene_bp5p[$i]+1)-1))."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$attributes exon_number \"0\";\n";
            print GTF2 "$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\t$tmp_line[1]\texon\t".($gene5p_length+1-($tmp_line[3]-$gene_bp5p[$i]))."\t".($gene5p_length+1-($tmp_line[3]-$gene_bp5p[$i]))."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$attributes exon_number \"0\";\n";
          }
        }
        elsif($tmp_line[3]<=$gene_bp5p[$i] && $tmp_line[4]>=$gene_bp5p[$i]){
          print GTF2 "$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\t$tmp_line[1]\t$tmp_line[2]\t".($gene5p_length+1-($gene5p_length))."\t".($gene5p_length+1)."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$tmp_line[8]";
          if($tmp_line[2] eq "transcript"){
            $attributes= $tmp_line[8];
            chop($attributes);
            print GTF2 "$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\t$tmp_line[1]\texon\t".($gene5p_length+1-($gene5p_length))."\t".($gene5p_length+1-($gene5p_length))."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$attributes exon_number \"0\";\n";
            print GTF2 "$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\t$tmp_line[1]\texon\t".($gene5p_length+1)."\t".($gene5p_length+1)."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$attributes exon_number \"0\";\n";    

          }
        }
      }
    }
  }
  @greplines = grep(/$gene_id3p[$i]/,@annotation_file);
  foreach $line (@greplines){
    @tmp_line = split("\t",$line);
    if($tmp_line[2] eq "gene"){
      $attributes = $tmp_line[8];
      chop($attributes);
      $attributes = $attributes." color=#20B2AA;\n";
      print GFF2 "$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\t$tmp_line[1]\t$tmp_line[2]\t".($gene5p_length+2)."\t".($gene5p_length+2+$gene3p_length)."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$attributes";
      print GTF2 "$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\t$tmp_line[1]\t$tmp_line[2]\t".($gene5p_length+2)."\t".($gene5p_length+2+$gene3p_length)."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$attributes";
    }else{
      if($strand2 eq "true"){
        if($tmp_line[3] >= $gene_bp3p[$i] && $tmp_line[4]>=$gene_bp3p[$i]){
          print GTF2 "$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\t$tmp_line[1]\t$tmp_line[2]\t".($gene5p_length+2+$tmp_line[3]-$gene_bp3p[$i])."\t".($gene5p_length+2+$tmp_line[4]-$gene_bp3p[$i])."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$tmp_line[8]";
          if($tmp_line[2] eq "transcript"){
                                                $attributes= $tmp_line[8];
                                                chop($attributes);
                                                print GTF2 "$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\t$tmp_line[1]\texon\t".($gene5p_length+2+$tmp_line[3]-$gene_bp3p[$i])."\t".($gene5p_length+2+$tmp_line[3]-$gene_bp3p[$i])."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$attributes exon_number \"0\";\n";
            print GTF2 "$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\t$tmp_line[1]\texon\t".($gene5p_length+2+$tmp_line[4]-$gene_bp3p[$i])."\t".($gene5p_length+2+$tmp_line[4]-$gene_bp3p[$i])."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$attributes exon_number \"0\";\n";
                                        }
        }elsif($tmp_line[3] <=$gene_bp3p[$i] && $tmp_line[4]>=$gene_bp3p[$i]){
          print GTF2 "$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\t$tmp_line[1]\t$tmp_line[2]\t".($gene5p_length+2)."\t".($gene5p_length+2+$tmp_line[4]-$gene_bp3p[$i])."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$tmp_line[8]";
          if($tmp_line[2] eq "transcript"){
                                                $attributes= $tmp_line[8];
                                                chop($attributes);
                                                print GTF2 "$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\t$tmp_line[1]\texon\t".($gene5p_length+2)."\t".($gene5p_length+2)."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$attributes exon_number \"0\";\n";
            print GTF2 "$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\t$tmp_line[1]\texon\t".($gene5p_length+2+$tmp_line[4]-$gene_bp3p[$i])."\t".($gene5p_length+2+$tmp_line[4]-$gene_bp3p[$i])."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$attributes exon_number \"0\";\n";
                                        }
        }
      }else{
        if($tmp_line[3] <=$gene_bp3p[$i] && $tmp_line[4]<=$gene_bp3p[$i]){
          print GTF2 "$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\t$tmp_line[1]\t$tmp_line[2]\t".($gene5p_length+$gene3p_length+2-($tmp_line[4]-$start3p))."\t".($gene5p_length+$gene3p_length+2-($tmp_line[3]-$start3p))."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$tmp_line[8]";
          if($tmp_line[2] eq "transcript"){
                                               $attributes= $tmp_line[8];
                                                chop($attributes);
                                                print GTF2 "$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\t$tmp_line[1]\texon\t".($gene5p_length+$gene3p_length+2-($tmp_line[4]-$start3p))."\t".($gene5p_length+$gene3p_length+2-($tmp_line[4]-$start3p))."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$attributes exon_number \"0\";\n";
            print GTF2 "$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\t$tmp_line[1]\texon\t".($gene5p_length+$gene3p_length+2-($tmp_line[3]-$start3p))."\t".($gene5p_length+$gene3p_length+2-($tmp_line[3]-$start3p))."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$attributes exon_number \"0\";\n";
                                        }
        }elsif($tmp_line[3] <=$gene_bp3p[$i] && $tmp_line[4]>=$gene_bp3p[$i]){
          print GTF2 "$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\t$tmp_line[1]\t$tmp_line[2]\t".($gene5p_length+$gene3p_length+2-$gene3p_length)."\t".($gene5p_length+$gene3p_length+2-($tmp_line[3]-$start3p))."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$tmp_line[8]";
          if($tmp_line[2] eq "transcript"){
                                                $attributes= $tmp_line[8];
                                                chop($attributes);
                                                print GTF2 "$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\t$tmp_line[1]\texon\t".($gene5p_length+$gene3p_length+2-$gene3p_length)."\t".($gene5p_length+$gene3p_length+2-$gene3p_length)."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$attributes exon_number \"0\";\n";
            print GTF2 "$gene_id5p[$i]-$gene_bp5p[$i]|$gene_id3p[$i]-$gene_bp3p[$i]\t$tmp_line[1]\texon\t".($gene5p_length+$gene3p_length+2-($tmp_line[3]-$start3p))."\t".($gene5p_length+$gene3p_length+2-($tmp_line[3]-$start3p))."\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$attributes exon_number \"0\";\n";
                                        }
        }
      }
    }  
  }
}
close VIRTUAL_REF1_H;
close VIRTUAL_REF2_H;
close GFF1;
close GFF2;
close GTF1;
close GTF2;



# Trim all the lines of the FASTAs at max 80 chars
$cmd = "fold -w 80 $OUT_DIR/tmp_virtual_ref1.fa > $OUT_DIR/virtual_ref1.fa";
system($cmd);
$cmd = "fold -w 80 $OUT_DIR/tmp_virtual_ref2.fa > $OUT_DIR/virtual_ref2.fa";
system($cmd);

# GENERATE GENOME INDEXES
print "Generating genome indexes...\n";
# virtual_ref1 genome indexes
$cmd = "STAR --runThreadN $threads --runMode genomeGenerate --genomeDir $GENOME_IDX1 --genomeFastaFiles $OUT_DIR/virtual_ref1.fa --sjdbGTFfile $OUT_DIR/annotation1.gtf";
print "$cmd\n";
system($cmd);
# virtual_ref2 genome indexes
$cmd = "STAR --runThreadN $threads --runMode genomeGenerate --genomeDir $GENOME_IDX2 --genomeFastaFiles $OUT_DIR/virtual_ref2.fa --sjdbGTFfile $OUT_DIR/annotation2.gtf";
print "$cmd\n";
system($cmd);

# ALIGNMENTS
# STAR alignment on virtual_ref1
if ($INPUT_FORMAT eq "gz") {
  print "STAR alignment on virtual_ref1\n";
  $cmd = "STAR --runThreadN $threads --genomeDir $GENOME_IDX1 --readFilesIn $reads1 $reads2 --readFilesCommand gunzip -k -c --outMultimapperOrder Random  --outSAMprimaryFlag AllBestScore --outFilterMultimapNmax 100 --outFilterScoreMinOverLread $AS --outFileNamePrefix $OUT_DIR/1. --seedPerWindowNmax 10";
  print "$cmd\n";
  system($cmd);
}else{
  print "STAR alignment on virtual_ref1\n";
  $cmd = "STAR --runThreadN $threads --genomeDir $GENOME_IDX1 --readFilesIn $reads1 $reads2 --outMultimapperOrder Random --outSAMprimaryFlag AllBestScore --outFilterMultimapNmax 100 --outFilterScoreMinOverLread $AS --outFileNamePrefix $OUT_DIR/1. --seedPerWindowNmax 10";
  print "$cmd\n";
  system($cmd);  
}
# STAR alignment on virtual_ref2
if ($INPUT_FORMAT eq "gz") {
 print "STAR alignment on virtual_ref2\n";
 $cmd = "STAR --runThreadN $threads --genomeDir $GENOME_IDX2 --readFilesIn $reads1 $reads2 --readFilesCommand gunzip -k -c --outMultimapperOrder Random --outSAMprimaryFlag AllBestScore --outFilterMultimapNmax 100 --outFilterScoreMinOverLread $AS --outFileNamePrefix $OUT_DIR/2. --seedPerWindowNmax 10";
  print "$cmd\n";
  system($cmd);
}else{
  print "STAR alignment on virtual_ref2\n";
  $cmd = "STAR --runThreadN $threads --genomeDir $GENOME_IDX2 --readFilesIn $reads1 $reads2 --outMultimapperOrder Random --outSAMprimaryFlag AllBestScore --outFilterMultimapNmax 100 --outFilterScoreMinOverLread $AS --outFileNamePrefix $OUT_DIR/2. --seedPerWindowNmax 10";
  print "$cmd\n";
  system($cmd);
}

# Find unique alignmensts of each read (best hit) for each template sequence
open TMP_ALIGN1_H, "<", "$OUT_DIR/1.Aligned.out.sam";
open TMP_UNIQUE_ALIGN1_H, ">", "$OUT_DIR/tmp_unique_align1.sam";
%fusion_mapping_already_present1;
%primary_alignment1;
while($line1 = <TMP_ALIGN1_H>){
  if($line1 !~ /^@/){
    $line2 = <TMP_ALIGN1_H>;
    @tmp_line1=split("\t",$line1);
    @tmp_line2=split("\t",$line2);
    if($tmp_line1[5] eq "\*"||$tmp_line2[5] eq "\*"){  #unmapped paired end reads (one of the two mate does not map at all)
      # do nothing -> change here if you want to see also unmapped reads
    }else{
      if(!defined $fusion_mapping_already_present1{"$tmp_line1[0]-$tmp_line1[2]"}){  #new entry
        $fusion_mapping_already_present1{"$tmp_line1[0]-$tmp_line1[2]"}=1;
        $primary_alignment1{"$tmp_line1[0]-$tmp_line1[2]-1"}=$line1;
        $primary_alignment1{"$tmp_line1[0]-$tmp_line1[2]-2"}=$line2;
      }else{
        @hash_line1=split("\t",$primary_alignment1{"$tmp_line1[0]-$tmp_line1[2]-1"});
        @hash_line2=split("\t",$primary_alignment1{"$tmp_line1[0]-$tmp_line1[2]-2"});
        $AS_hashline1=(split(/:/,$hash_line1[13]))[-1];
        $AS_hashline2=(split(/:/,$hash_line2[13]))[-1];
        $AS_tmpline1=(split(/:/,$tmp_line1[13]))[-1];
        $AS_tmpline2=(split(/:/,$tmp_line2[13]))[-1];

        if(($AS_hashline1+$AS_hashline2)<($AS_tmpline1+$AS_tmpline2)){
          $primary_alignment1{"$tmp_line1[0]-$tmp_line1[2]-1"}=$line1;
          $primary_alignment1{"$tmp_line1[0]-$tmp_line1[2]-2"}=$line2;
        }
      }
    }
  }else{
    print TMP_UNIQUE_ALIGN1_H $line1;  #SAM header line
  }
}
foreach $key (sort keys %primary_alignment1) {
  print TMP_UNIQUE_ALIGN1_H $primary_alignment1{$key};
}

close TMP_ALIGN1_H;
close TMP_UNIQUE_ALIGN1_H;

open TMP_ALIGN2_H, "<", "$OUT_DIR/2.Aligned.out.sam";
open TMP_UNIQUE_ALIGN2_H, ">", "$OUT_DIR/tmp_unique_align2.sam";
%fusion_mapping_already_present2;
%primary_alignment2;
while($line1 = <TMP_ALIGN2_H>){
  if($line1 !~ /^@/){
    $line2 = <TMP_ALIGN2_H>;
    @tmp_line1=split("\t",$line1);
    @tmp_line2=split("\t",$line2);
    if($tmp_line1[5] eq "\*"||$tmp_line2[5] eq "\*"){       #unmapped paired end reads (one of the two mate does not map at all)
      # do nothing -> change here if you want to see also unmapped reads
    }else{  
       if(!defined $fusion_mapping_already_present2{"$tmp_line1[0]-$tmp_line1[2]"}){    #new entry
         $fusion_mapping_already_present2{"$tmp_line1[0]-$tmp_line1[2]"}=1;
         $primary_alignment2{"$tmp_line1[0]-$tmp_line1[2]-1"}=$line1;
         $primary_alignment2{"$tmp_line1[0]-$tmp_line1[2]-2"}=$line2;
       }else{  
         @hash_line1=split("\t",$primary_alignment2{"$tmp_line1[0]-$tmp_line1[2]-1"});
         @hash_line2=split("\t",$primary_alignment2{"$tmp_line1[0]-$tmp_line1[2]-2"});
         $AS_hashline1=(split(/:/,$hash_line1[13]))[-1];
         $AS_hashline2=(split(/:/,$hash_line2[13]))[-1];
         $AS_tmpline1=(split(/:/,$tmp_line1[13]))[-1];
         $AS_tmpline2=(split(/:/,$tmp_line2[13]))[-1];
       
         if(($AS_hashline1+$AS_hashline2)<($AS_tmpline1+$AS_tmpline2)){
            $primary_alignment2{"$tmp_line1[0]-$tmp_line1[2]-1"}=$line1;
            $primary_alignment2{"$tmp_line1[0]-$tmp_line1[2]-2"}=$line2;
         }
       }
     }
   }else{
      print TMP_UNIQUE_ALIGN2_H $line1;       #SAM header line
   }
 }
 foreach $key (sort keys %primary_alignment2) {
   print TMP_UNIQUE_ALIGN2_H $primary_alignment2{$key};
}

close TMP_ALIGN2_H;
close TMP_UNIQUE_ALIGN2_H;


$cmd = "samtools view -S -b $OUT_DIR/tmp_unique_align1.sam > $OUT_DIR/align1.bam";
print "$cmd\n";
system($cmd);
$cmd= "samtools sort $OUT_DIR/align1.bam > $OUT_DIR/align1.sorted.bam";
print "$cmd\n";
system($cmd);
$cmd = "samtools index $OUT_DIR/align1.sorted.bam";
print "$cmd\n";
system($cmd);

$cmd = "samtools view -S -b $OUT_DIR/tmp_unique_align2.sam > $OUT_DIR/align2.bam";
print "$cmd\n";
system($cmd);
$cmd= "samtools sort $OUT_DIR/align2.bam > $OUT_DIR/align2.sorted.bam";
print "$cmd\n";
system($cmd);
$cmd = "samtools index $OUT_DIR/align2.sorted.bam";
print "$cmd\n";
system($cmd);


# getGenomeSeq SUBROUTINE
#sub getGenomeSeq {
#  my ($slice_adaptor,$chr,$start,$end,$strand) = @_;  
#  my $slice = $slice_adaptor->fetch_by_region( 'chromosome', $chr, $start, $end );
#  if($strand eq "true"){
#          $seq = $slice->seq();
#  }else{  
#          $slice_inv = $slice->invert();
#          $seq = $slice_inv->seq();
#  }
#  return $seq;

#sub getGenomeSeq {
#  my ($chr,$begin,$end) = @_;
#  open(SAMTOOLS,"/path/to/samtools faidx /path/to/reference.fasta $seqName:$begin-$end |");
#  while(my $line = <SAMTOOLS>){
#    $seq = $line;
#  }
#  close(SAMTOOLS);
#  return $seq;

sub getGenomeSeq {
  use Bio::Seq;
  use Bio::DB::Fasta;
  my ($chr,$start,$end,$fwd_strand,$fasta) = @_; 
  #print "in getGenomeSeq: $chr $start $end $fwd_strand $fasta\n";
  my $db = Bio::DB::Fasta->new($fasta);
  if($strand eq "true"){
    $mySeq = $db->seq($chr, $start => $end);
  }else{  
    $mySeq = $db->seq($chr, $end => $start);
  }
  return $mySeq
}
