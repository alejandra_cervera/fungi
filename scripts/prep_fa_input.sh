#!/bin/bash

set -e

_helpexit() {
  printf '
Usage: %s \
  -i fusion-caller-outputs  \
  -o fusion-analyzer-input-file

- fusion-caller-outputs: File containing the paths to the output folders produced by fungi-fusion-caller.
- fusion-analyzer-input-file: The name for the output file created with this script. It will produce a file with 3 columtns (Sample, Tool, File) that can be used as input for fungi-fusion-nalyzer. 

This script only works for output folders created with fungi-fusion-caller.

' "$SELFNAME"
    exit
}


[[ -z "$1" ]] && _helpexit
for (( i=1; i<=$#; i++ )); do
    [[ ${!i} = "-h" ]] && _helpexit
    [[ ${!i} = "--help" ]] && _helpexit
done
for (( i=1; i<=$#; i++ )); do
  j=$(( $i + 1 ))
  [[ ${!i} = "-i" ]] && fusion_caller_outputs=${!j}
  [[ ${!i} = "-o" ]] && analyzer_input_file=${!j}
  i=$(( i + 1 ))
done

echo -e "Sample\tTool\tFile" > ${analyzer_input_file}
for i in $( cat ${fusion_caller_outputs} )
do
  sample=$( cat $i/.sample )
  tool=$( cat $i/.tool )
  final=${i}/final
  echo -e $sample"\t"$tool"\t"${final} >> ${analyzer_input_file}
done
