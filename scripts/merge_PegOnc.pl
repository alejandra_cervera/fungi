#!/usr/bin/perl
use File::Path;

my ($PEGASUS_DIR, $ONCOFUSE_DIR,$OUT_DIR, $TH1, $TH2, $DB)=@ARGV;

$tmp_dir = "$OUT_DIR/tmp";
mkdir $tmp_dir;

#field 1 is pegasus score, 17 is bp first gene, 18 bp second gene, 3 is sample with method (ex. sample1_chimerascan)
$cmd = "tail -n +2 $PEGASUS_DIR/pegasus.output.txt| sort -u -k1,1r -k17,17 -k18,18 -k3,3 > $tmp_dir/pegasus_out.sorted.txt";
system($cmd);

$cmd = "tail -q -n +2 $ONCOFUSE_DIR/* > $tmp_dir/oncofuse.merge.csv";
system($cmd);
$cmd = "sort -u -k23,23rg -k6,6 $tmp_dir/oncofuse.merge.csv > $tmp_dir/oncofuse.summary.csv";
system($cmd);

open PEGASUS_REP_H, "<","$tmp_dir/pegasus_out.sorted.txt" or die $!;
@PEG_REP = <PEGASUS_REP_H>;
close PEGASUS_REP_H;

open ONCOFUSE_REP_H, "<", "$tmp_dir/oncofuse.summary.csv" or die $!;
@ONCO_REP = <ONCOFUSE_REP_H>;
close ONCOFUSE_REP_H;

open DB_REP_H, "<", "$DB" or die $!; 
#@DB_REP = <DB_REP_H>;
my %hash;
while (my $line=<DB_REP_H>) {

  chomp $line;
  print $line,"\n";
  (my $fusion, my $db) = split /\t/, $line;

  $hash{$fusion} = $db;
}

#print "after chomp \n";
# Print hash for testinf purposes
#while ( my ($k,$v) = each %hash ) {
#      print "Key $k => $v\n";
#}
close DB_REP_H;

open OUT_H, ">", "$OUT_DIR/final.candidates.csv";
print OUT_H "FusionName\tPegasus_score\tOncofuse_score\tgene5p_name\tgene3p_name\tgene5p_id\tgene3p_id\tbp_coordinates\tSample\tEncompassing reads\tSplit reads\tSample_occurrence_list\tKinase_info\tTranscript_ID1\tTranscript_ID2\tReading_frame_info\tProtein_start1\tProtein_end1\tProtein_start2\tProtein_end2\tProtein_sequence\tExon_Gene1\tExon_Gene2\tBreakpoint_region1\tBreakpoint_reagion2\tConserved_Domain1\tLost_Domain1\tConserved_Domain2\tLost_Domain2\tDB\n";

foreach $peg_line (@PEG_REP){
	@tmp_line1=split("\t",$peg_line);
	$peg_fus= "chr$tmp_line1[6]:$tmp_line1[16]>chr$tmp_line1[7]:$tmp_line1[17]";
  $fusionId=join("_",$tmp_line1[14],$tmp_line1[15]);
  $ensemblId1=$tmp_line1[18];
  $ensemblId2=$tmp_line1[19];
  chop($ensemblId1);
  chop($ensemblId2);
	if($tmp_line1[0]>=$TH1){
		$match_found=0;
		foreach $onco_line (@ONCO_REP){
			@tmp_line2=split("\t",$onco_line);
      if($peg_fus eq $tmp_line2[5]){
				$match_found=1;
        print OUT_H "$tmp_line1[14]--$tmp_line1[15]\t$tmp_line1[0]\t$tmp_line2[22]\t$tmp_line1[14]\t$tmp_line1[15]\t$ensemblId1\t$ensemblId2\t$tmp_line2[5]\t$tmp_line1[2]\t$tmp_line1[4]\t$tmp_line1[5]\t$tmp_line1[21]\t$tmp_line1[23]\t$tmp_line1[24]\t$tmp_line1[25]\t$tmp_line1[26]\t$tmp_line1[27]\t$tmp_line1[28]\t$tmp_line1[29]\t$tmp_line1[30]\t$tmp_line1[31]\t$tmp_line1[32]\t$tmp_line1[33]\t$tmp_line1[34]\t$tmp_line1[35]\t$tmp_line2[24]\t$tmp_line2[26]\t$tmp_line2[25]\t$tmp_line2[27]\t$hash{$fusionId}\n";
     	}
		}
		if($match_found==0){
      print OUT_H "$tmp_line1[14]--$tmp_line1[15]\t$tmp_line1[0]\t\t$tmp_line1[14]\t$tmp_line1[15]\t$ensemblId1\t$ensemblId2\t$peg_fus\t$tmp_line1[2]\t$tmp_line1[4]\t$tmp_line1[5]\t$tmp_line1[21]\t$tmp_line1[23]\t$tmp_line1[24]\t$tmp_line1[25]\t$tmp_line1[26]\t$tmp_line1[27]\t$tmp_line1[28]\t$tmp_line1[29]\t$tmp_line1[30]\t$tmp_line1[31]\t$tmp_line1[32]\t$tmp_line1[33]\t$tmp_line1[34]\t$tmp_line1[35]\t$tmp_line2[24]\t$tmp_line2[26]\t$tmp_line2[25]\t$tmp_line2[27]\t$hash{$fusionId}\n";
    }
	}
}
#  else{
#		foreach $onco_line (@ONCO_REP){
#			@tmp_line2=split("\t",$onco_line);
#			if($peg_fus eq $tmp_line2[5]){
#        if ($tmp_line1[0]<$TH1){
#					print OUT_H "$tmp_line1[0]\t$tmp_line2[22]\t$tmp_line1[14]\t$tmp_line1[15]\t$tmp_line1[18]\t$tmp_line1[19]\t$tmp_line2[5]\t$tmp_line1[2]\t$tmp_line1[4]\t$tmp_line1[5]\t$tmp_line1[21]\t$tmp_line1[23]\t$tmp_line1[24]\t$tmp_line1[25]\t$tmp_line1[26]\t$tmp_line1[27]\t$tmp_line1[28]\t$tmp_line1[29]\t$tmp_line1[30]\t$tmp_line1[31]\t$tmp_line1[32]\t$tmp_line1[33]\t$tmp_line1[34]\t$tmp_line1[35]\t$tmp_line2[24]\t$tmp_line2[26]\t$tmp_line2[25]\t$tmp_line2[27]\t$hash{$fusionId}\n";
 #       }
			#}
#		}
#	}
#}

foreach $onco_line (@ONCO_REP){
	@tmp_line2=split("\t",$onco_line);
  if($tmp_line2[22]>=$TH2){
		foreach $peg_line(@PEG_REP){
			@tmp_line1=split("\t",$peg_line);
			$peg_fus= "chr$tmp_line1[6]:$tmp_line1[16]>chr$tmp_line1[7]:$tmp_line1[17]";
  		$fusionId=join("_",$tmp_line1[14],$tmp_line1[15]);
  		$ensemblId1=$tmp_line1[18];
  		$ensemblId2=$tmp_line1[19];
      chop($ensemblId1);
      chop($ensemblId2);
			if($peg_fus eq $tmp_line2[5]){
        if ($tmp_line1[0]<$TH1){
          print OUT_H "$tmp_line1[14]--$tmp_line1[15]\t$tmp_line1[0]\t$tmp_line2[22]\t$tmp_line1[14]\t$tmp_line1[15]\t$ensemblId1\t$ensemblId2\t$tmp_line2[5]\t$tmp_line1[2]\t$tmp_line1[4]\t$tmp_line1[5]\t$tmp_line1[21]\t$tmp_line1[23]\t$tmp_line1[24]\t$tmp_line1[25]\t$tmp_line1[26]\t$tmp_line1[27]\t$tmp_line1[28]\t$tmp_line1[29]\t$tmp_line1[30]\t$tmp_line1[31]\t$tmp_line1[32]\t$tmp_line1[33]\t$tmp_line1[34]\t$tmp_line1[35]\t$tmp_line2[24]\t$tmp_line2[26]\t$tmp_line2[25]\t$tmp_line2[27]\t$hash{$fusionId}\n";
        }
			}
		}
	}
}

close OUT_H;
