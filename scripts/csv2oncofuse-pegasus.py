#!/usr/bin/env python3

import sys
import argparse
import csv
import os
from pprint import pprint


def get_options():
    parser = argparse.ArgumentParser(description="Import files for Fusion Analyzer")
    parser.add_argument("--input")
    parser.add_argument("--pegasus")
    parser.add_argument("--oncofuse")
    parser.add_argument("--tissue")
    parser.add_argument("--sample_keys_file")

    options = parser.parse_args()
    return options


def read_input(filename):
    with open(filename, "rt") as fp:
        reader = csv.DictReader(fp, dialect="excel-tab")
        data = [row for row in reader]
    return data


def write_pegasus(data, filename):
    with open(filename, "wt") as fp:
        writer = csv.DictWriter(
            fp,
            fieldnames="#5p_symbol 5p_ensembl 5p_strand 5p_chr 3p_symbol 3p_ensembl 3p_strand 3p_chr 5p_start 5p_end bp 3p_start 3p_end split_reads tot_reads".split(),
            dialect="excel-tab",
            quoting=csv.QUOTE_NONE,
        )
        writer.writeheader()

        for row in data:
            (gene_start1, gene_end1, gene_strand1) = row["gene1_coord"].split(":")
            (gene_start2, gene_end2, gene_strand2) = row["gene2_coord"].split(":")
            (tmp_coord1, tmp_coord2) = row["bp"].split("-")
            (gene_chr1, gene_bp1) = tmp_coord1.split(":")
            (gene_chr2, gene_bp2) = tmp_coord2.split(":")

            if gene_strand1 in ("1", "+"):
                gene_end1 = gene_bp1
                gene_strand1 = "+"
            else:
                gene_start1 = gene_bp1
                gene_strand1 = "-"

            if gene_strand2 in ("-1", "-"):
                gene_end2 = gene_bp2
                gene_strand2 = "-"
            else:
                gene_start2 = gene_bp2
                gene_strand2 = "+"

            writer.writerow(
                {
                    "#5p_symbol": row["gene_name1"],
                    "5p_ensembl": row["ensembl_gene_id1"],
                    "5p_strand": gene_strand1,
                    "5p_chr": gene_chr1,
                    "3p_symbol": row["gene_name2"],
                    "3p_ensembl": row["ensembl_gene_id2"],
                    "3p_strand": gene_strand2,
                    "3p_chr": gene_chr2,
                    "5p_start": gene_start1,
                    "5p_end": gene_end1,
                    "bp": "|",
                    "3p_start": gene_start2,
                    "3p_end": gene_end2,
                    "split_reads": " spanning: {}".format(row["junction_reads"]),
                    "tot_reads": " encompassing: {}".format(row["spanning_reads"]),
                }
            )


def write_oncofuse(data, filename, tissue):
    with open(filename, "wt") as fp:
        writer = csv.DictWriter(
            fp,
            fieldnames="#chr1 coord1 chr2 coord2 tissue spanning encompassing".split(),
            dialect="excel-tab",
            quoting=csv.QUOTE_NONE,
        )
        writer.writeheader()
        for row in data:
            (gene_start1, gene_end1, gene_strand1) = row["gene1_coord"].split(":")
            (gene_start2, gene_end2, gene_strand2) = row["gene2_coord"].split(":")
            (tmp_coord1, tmp_coord2) = row["bp"].split("-")
            (gene_chr1, gene_bp1) = tmp_coord1.split(":")
            (gene_chr2, gene_bp2) = tmp_coord2.split(":")

            writer.writerow(
                {
                    "#chr1": gene_chr1,
                    "coord1": gene_bp1,
                    "chr2": gene_chr2,
                    "coord2": gene_bp2,
                    "tissue": tissue,
                    "spanning": row["junction_reads"],
                    "encompassing": row["spanning_reads"],
                }
            )


if __name__ == "__main__":
    opts = get_options()
    i = 1
    fkeys = {}
    for f in os.listdir(opts.input):
        if f.startswith("."):
            continue
        short_filename = "s{}.csv".format(str(i))
        fkeys[short_filename] = f
        i += 1
        data = read_input(os.path.join(opts.input, f))
        write_pegasus(data, os.path.join(opts.pegasus, short_filename))
        write_oncofuse(data, os.path.join(opts.oncofuse, short_filename), opts.tissue)
    with open(opts.sample_keys_file, "w") as f:
        print(fkeys, file=f)
