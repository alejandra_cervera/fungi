#!/usr/bin/env python3

import sys
import argparse
import csv
from pprint import pprint

def get_options():
    parser = argparse.ArgumentParser(
        description="Import files for Fusion Analyzer"
    )
    parser.add_argument('--tool')
    parser.add_argument('--input')
    parser.add_argument('--ID')
    parser.add_argument('--min_reads', type = int)
    parser.add_argument('--output')
    parser.add_argument('--discarded')

    options=parser.parse_args()
    return options


def read_input(opts):
    data = read_file(opts.input)
    if opts.tool == "arriba":
        imported = import_arriba(opts, data)
    elif opts.tool == "chimerascan":
        imported = import_chimerascan(opts, data)
    elif opts.tool == "defuse":
        imported = import_defuse(opts, data)
    elif opts.tool == "ericscript":
        imported = import_ericscript(opts, data)
    elif opts.tool == "fusioncatcher":
        imported = import_fusioncatcher(opts, data)
    elif opts.tool == "soapfuse":
        imported = import_soapfuse(opts, data)
    elif opts.tool == "starfusion":
        imported = import_starfusion(opts, data)
    elif opts.tool == "generic":
        imported = data
    else:
        print("Tool name {} not recognized".format(opts.tool))
        sys.exit(1)

    filtered, filtered_out = filter_data(opts.min_reads, imported)

    return filtered, filtered_out


def read_file(filename):
    with open(filename, 'rt') as fp:
        reader = csv.DictReader(fp, dialect='excel-tab')
        data = []
        for row in reader:
            data.append(row)
    return data


def import_arriba(opts, data):
    imported = []
    for row in data:
        values = {
          'gene_name1': row['#gene1'],
          'gene_name2': row['gene2'],
          'gene_id1': "",
          'gene_id2': "",
          'chr1': row['breakpoint1'].split(":")[0],
          'bp1': row['breakpoint1'].split(":")[1],
          'gene_strand1': row['strand1(gene/fusion)'].split("/")[0],
          'chr2': row['breakpoint2'].split(":")[0],
          'bp2': row['breakpoint2'].split(":")[1],
          'gene_strand2': row['strand2(gene/fusion)'].split("/")[0],
          'junction_reads': int(row['split_reads1']) + int(row['split_reads2']),
          'spanning_reads': int(row['discordant_mates']),
          'sample': opts.ID,
          'method': opts.tool,
          'reason': 'OK'
        }

        imported.append(values)
    return imported


def import_defuse(opts, data):
    imported = []
    for row in data:
        values = {
          'gene_name1': row['gene_name1'],
          'gene_name2': row['gene_name2'],
          'gene_id1': row['gene1'],
          'gene_id2': row['gene2'],
          'chr1': row['gene_chromosome1'],
          'bp1': row['genomic_break_pos1'],
          'gene_strand1': row['gene_align_strand1'],
          'chr2': row['gene_chromosome2'],
          'bp2': row['genomic_break_pos2'],
          'gene_strand2': row['gene_align_strand2'],
          'junction_reads': int(row['splitr_count']),
          'spanning_reads': int(row['span_count']),
          'sample': opts.ID,
          'method': opts.tool,
          'reason': 'OK'
        }

        imported.append(values)
    return imported


def import_starfusion(opts, data):
    imported = []
    for row in data:
        fusion1 = row['LeftBreakpoint'].split(":")
        fusion2 = row['RightBreakpoint'].split(":")
        if len(fusion1) == 2:
            fusion1.append("")
        if len(fusion2) == 2:
            fusion2.append("")
        values = {
          'gene_name1': row['LeftGene'].split("^")[0],
          'gene_name2': row['RightGene'].split("^")[0],
          'gene_id1': row['LeftGene'].split("^")[1].split(".")[0],
          'gene_id2': row['RightGene'].split("^")[1].split(".")[0],
          'chr1': fusion1[0].replace("chr",""),
          'bp1': fusion1[1],
          'gene_strand1': fusion1[2],
          'chr2': fusion2[0].replace("chr",""),
          'bp2': fusion2[1],
          'gene_strand2': fusion2[2],
          'junction_reads': int(row['JunctionReadCount']),
          'spanning_reads': int(row['SpanningFragCount']),
          'sample': opts.ID,
          'method': opts.tool,
          'reason': 'OK'
        }

        imported.append(values)
    return imported


def import_chimerascan(opts, data):
    imported = []
    for row in data:
        values = {
          'gene_name1': row['genes5p'],
          'gene_name2': row['genes3p'],
          'gene_id1': "",
          'gene_id2': "",
          'chr1': row['#chrom5p'],
          'bp1': None,
          'gene_strand1': row['strand5p'],
          'chr2': row['chrom3p'],
          'bp2': None,
          'gene_strand2': row['strand3p'],
          'junction_reads': int(row['spanning_frags']),
          'spanning_reads': row['total_frags'],
          'sample': opts.ID,
          'method': opts.tool,
          'reason': 'OK'
        }
        values['chr1'] = values['chr1'].replace('chr','')
        values['chr2'] = values['chr2'].replace('chr','')
        try:
            values['spanning_reads'] = int(values['spanning_reads'])
        except ValueError:
            #if tot_cnt is a string and not number we make it zero
            values['spanning_reads'] = 0
        if values['gene_strand1'] == "+":
            values['bp1'] = int(row['start5p']) + 1
        else:
            values['bp1'] = int(row['end5p']) + 1
        if values['gene_strand2'] == "-":
            values['bp2'] = int(row['start3p']) + 1
        else:
            values['bp2'] = int(row['end3p']) + 1

        gene_names1 = values['gene_name1'].split(",")
        gene_names2 = values['gene_name2'].split(",")
        if len(gene_names1) > 1 or len(gene_names2) > 1:
            for name1 in gene_names1:
                for name2 in gene_names2:
                    sub_value = values.copy()
                    sub_value['gene_name1'] = name1
                    sub_value['gene_name2'] = name2
                    imported.append(sub_value)
        else:
            # just one gene pair
            imported.append(values)
    return imported


def import_ericscript(opts, data):
    imported = []
    for row in data:
        values = {
          'gene_name1': row['GeneName1'],
          'gene_name2': row['GeneName2'],
          'gene_id1': row['EnsemblGene1'],
          'gene_id2': row['EnsemblGene2'],
          'chr1': row['chr1'],
          'bp1': row['Breakpoint1'],
          'gene_strand1': row['strand1'],
          'chr2': row['chr2'],
          'bp2': row['Breakpoint2'],
          'gene_strand2': row['strand2'],
          'junction_reads': int(row['crossingreads']),
          'spanning_reads': int(row['spanningreads']),
          'sample': opts.ID,
          'method': opts.tool,
          'reason': 'OK'
        }

        imported.append(values)
    return imported


def import_fusioncatcher(opts, data):
    imported = []
    for row in data:
        fusion1 = row['Fusion_point_for_gene_1(5end_fusion_partner)'].split(":")
        fusion2 = row['Fusion_point_for_gene_2(3end_fusion_partner)'].split(":")
        values = {
          'gene_name1': row['Gene_1_symbol(5end_fusion_partner)'],
          'gene_name2': row['Gene_2_symbol(3end_fusion_partner)'],
          'gene_id1': row['Gene_1_id(5end_fusion_partner)'],
          'gene_id2': row['Gene_2_id(3end_fusion_partner)'],
          'chr1': fusion1[0],
          'bp1': fusion1[1],
          'gene_strand1': fusion1[2],
          'chr2': fusion2[0],
          'bp2': fusion2[1],
          'gene_strand2': fusion2[2],
          # ~ 'junction_reads': int(row['Counts_of_common_mapping_reads']),
           # fusioncatcher does not report junction and spanning reads separately
          'junction_reads': int(row['Spanning_pairs']),
          'spanning_reads': int(row['Spanning_pairs']),
          'sample': opts.ID,
          'method': opts.tool,
          'reason': 'OK'
        }

        imported.append(values)
    return imported


def import_soapfuse(opts, data):
    imported = []
    for row in data:
        gene1 = row['up_gene'].split("_")
        gene2 = row['dw_gene'].split("_")
        gene1.append("")
        gene2.append("")
        values = {
          'gene_name1': gene1[0],
          'gene_name2': gene2[0],
          #up_gene and dw_gene rarely include the id
          'gene_id1': gene1[1],
          'gene_id2': gene2[1],
          'chr1': row['up_chr'].replace("chr",""),
          'bp1': row['up_Genome_pos'],
          'gene_strand1': row['up_strand'],
          'chr2': row['dw_chr'].replace("chr",""),
          'bp2': row['dw_Genome_pos'],
          'gene_strand2': row['dw_strand'],
          'junction_reads': int(row['Junc_reads_num']),
          'spanning_reads': int(row['Span_reads_num']),
          'sample': opts.ID,
          'method': opts.tool,
          'reason': 'OK'
        }

        imported.append(values)
    return imported


def filter_data(min_reads, data):
    imported = []
    filtered_out = []
    for values in data:
        if int(values['junction_reads']) >= min_reads:
            imported.append(values)
        else:
            values['reason'] = "junction read count below threshold"
            filtered_out.append(values)
    return imported, filtered_out


def write_table(filename, data):
    with open(filename, 'wt') as fp:
        writer = csv.DictWriter(
            fp,
            fieldnames=get_fields(),
            dialect='excel-tab',
            quoting=csv.QUOTE_NONE
        )
        writer.writeheader()
        writer.writerows(data)


def get_fields():
    return "gene_name1 gene_name2 gene_id1 gene_id2 chr1 bp1 gene_strand1 chr2 bp2 gene_strand2 junction_reads spanning_reads sample method reason".split(" ")


if __name__ == "__main__":
    opts = get_options()
    filtered, filtered_out = read_input(opts)
    write_table(opts.output, filtered)
    write_table(opts.discarded, filtered_out)
