#!/bin/bash

set -e
set -u

# Polishes the results from Pegasus and Oncofuse
# Sample column should be renamed to Source (remove .csv ending)
# Sample_occurrence_list should be split into Sample_occurrence and Method_occurrence
# input is merged oncofuse and pegasus results (tmpDir/final.candidate.csv)
# key file to revert sample names to original names and tmpdir are needed
# output is final_candidates.csv in the OUTPUT folder

# Sample is field $9, Sample_occurrence_list is field $12

# steps:
# remove quotes
# remove trailing commas
# Rename Sample->Source, convert Sample_occurrence_list into Sample_occurrence and Methods

in_fusions=$1
in_keys=$2
tmpdir=$3
out_fusions=$4

sed 's/, /\n/g' ${in_keys} | tr -d "'" | tr -d ':' | tr ' ' '\t' | tr -d '{' | tr -d '}' > $tmpdir/master.txt
awk -F"\t" 'FNR==NR { array[$1]=$2; next } { for (i in array) gsub(i, array[i]) }1' OFS="\t" $tmpdir/master.txt ${in_fusions} > $tmpdir/final_renamed.csv

tr -d '"' < $tmpdir/final_renamed.csv | sed 's/,\t/\t/g' | awk -F"\t" '{if (NR==1) {$9="Source"; $12="Sample_occurrence\tMethods";} else {$9=gensub(".csv","","g",$9); $12=gensub(".csv","","g",$12); n=split($12,a,","); delete m; delete s; for (i=1; i<=n; i++) {k=split(a[i],b,"_"); m[b[k]]=b[k]; p=gensub("_"b[k],"",1,a[i]); s[p]=p;} l=""; q=""; for (i in m) {q=q","m[i];} for (i in s) {l=l","s[i];} $12=l"\t"q;} print $0}' OFS="\t" | sed 's/\t,/\t/g' | sed -e "s/\r//g" > ${out_fusions}


