
FUNGI_VERSION="release 2"

_banner() {
  {
  rowlen=$(( ${#1} + 4 ))
  eval printf "=%.0s" {1..${rowlen}}
  printf "\n  ${CG}%s${CZ}  \n" \
   "$1"
  eval printf "=%.0s" {1..${rowlen}}
  printf "\n"
  } >&2
}

_check_config() {
    if [[ -z "$config" ]]; then
        _error "Must use a config file"
    fi
    if [[ ! -e "$config" ]]; then
        # create template
        cat <<'EOF' > "$config"
# [Tool paths]
# Add the paths of the tools you want to use
# Minimal installation:
# - fungi-fusion-caller: whichever caller you want to use
# - fungi-fuion-analyzer: FusionCatcher, R, Pegasus and Oncofuse
# - fungi-fusion-visualizer: STAR, STAR-Fusion if you want to use FusionInspector
#STAR aligner needs to be in $PATH for several of the tools

#For Arriba and for fungi-fusion-visualizer the following paths are needed
STAR_HOME=/path/to/example/STAR-2.7.4a/
STAR_INDEX=/path/to/STAR-index-files
REF_GENOME=$STAR_INDEX/genome.fa
ANNOTATION=$STAR_INDEX/annotation.gtf

ARRIBA_HOME=/path
# Script will look for: ${ARRIBA_HOME}/run_arriba.sh
# Only change the next 3 if you want something different
ARRIBA_BLACKLIST=${ARRIBA_HOME}/database/blacklist_hg38_GRCh38_2018-11-04.tsv.gz
ARRIBA_GENOME=$REF_GENOME
ARRIBA_ANNOT=$ANNOTATION

#star-fusion home and db are used both by fusioncaller and fusionvisualizer
STARFUSION_HOME=/path
# Script will look for:  $STARFUSION_HOME/STAR-Fusion & $STARFUSION_HOME/FusionInspector/FusionInspector (binaries)
STARFUSION_DB=$STARFUSION_HOME/GRCh38_gencode_v33_CTAT_lib_Apr062020.source/ctat_genome_lib_build_dir/
# Corresponding db created/downloaded following tool instructions
# Trinity install PATH for fusionvisualizer
TRINITY_HOME=/path/to/trinityrnaseq-v2.10.0/

FUSIONCATCHER_HOME=/path/to/fusioncatcher
# Script will look for:  $FUSIONCATCHER_HOME/bin
FUSIONCATCHER_DB=$FUSIONCATCHER_HOME/data//human_v98/
# Corresponding db created/downloaded following tool instructions

CHIMERASCAN_HOME=/path
# Script will look for: $CHIMERASCAN_HOME/bin/chimerascan_run.py
CHIMERASCAN_DB=$CHIMERASCAN_HOME/db
# Corresponding db created/downloaded following tool instructions

DEFUSE_HOME=/path
# Script will look for: $DEFUSE_HOME"/scripts/defuse_run.pl
DEFUSE_DB=$DEFUSE_HOME/db
# Corresponding db created/downloaded following tool instructions

SOAPFUSE_HOME=/path
# Script will look for: $SOAPFUSE_HOME"/SOAPfuse-RUN.pl
# no DB needed

ERICSCRIPT_HOME=/path
# Script will look for: $ERICSCRIPT_HOME/ericscript.pl
ERICSCRIPT_DB=$ERICSCRIPT_HOME/ericscript_db_homosapiens_ensembl84
# Corresponding db created/downloaded following tool instructions

PEGASUS_HOME=/path/to/pegasus
# Script will look for eg.: $PEGASUS_HOME/resources/hsqldb-2.2.7/hsqldb/mydb
# and $PEGASUS_HOME/jars/QueryFusionDatabase.jar

ONCOFUSE_HOME=/path/to/oncofuse
# Script will look for: "$ONCOFUSE_HOME/Oncofuse.jar"

EOF
        echo "Config template written to: $config"
        echo "Edit and restart"
        exit
    fi

    source "$config"
    export ARRIBA_HOME
    export CHIMERASCAN_HOME
    export DEFUSE_HOME
    export ERICSCRIPT_HOME
    export FUSIONCATCHER_HOME
    export ONCOFUSE_HOME
    export PEGASUS_HOME
    export SOAPFUSE_HOME
    export STARFUSION_HOME
    export TRINITY_HOME
    export PATH="$STAR_HOME:$PATH"

}

_check_tool_file() {
    check_file="${!1}"/"$2"
    if [[ ! -e "$check_file" ]]; then
        err_msg="Can not find \$${1}/${2} -> ${!1}/${2}. Check config: $config"
        if [[ -n "$3" ]]; then
            err_msg="$3"
        fi
        _error "$err_msg"
    fi
}

_check_tool_env() {
    case "$1" in
        fusioncatcher)
            _check_tool_file FUSIONCATCHER_DB organism.txt \
                "Fusion Catcher requires \$FUSIONCATCHER_DB to point to folder with organism.txt file"
            _check_tool_file FUSIONCATCHER_HOME bin/label_fusion_genes.py
        ;;
        chimerascan)
            _check_tool_file CHIMERASCAN_HOME bin/chimerascan_run.py
        ;;
        defuse)
            _check_tool_file DEFUSE_HOME scripts/defuse_run.pl
        ;;
        soapfuse)
            _check_tool_file SOAPFUSE_HOME SOAPfuse-RUN.pl
        ;;
        eriscript)
            _check_tool_file ERICSCRIPT_HOME eriscript.pl
        ;;
        starfusion)
            _check_tool_file STARFUSION_HOME STAR-Fusion
            _check_tool_file STARFUSION_HOME FusionInspector
        ;;
        arriba)
            _check_tool_file ARRIBA_HOME run_arriba.sh
            _check_tool_file STAR_HOME STAR
        ;;
        pegasus)
            _check_tool_file PEGASUS_HOME jars/QueryFusionDatabase.jar ""
        ;;
        oncofuse)
            _check_tool_file ONCOFUSE_HOME Oncofuse.jar ""
        ;;
    esac
}

_error() {
  printf "${CR}%s${CZ}\n" \
   "$1" >&2
  exit 1
}

_print_title() {
    cat <<'EOF'
  n                                            n
 / `\                                        /` \ n
(___:)      _____                   .__     (___:/ '\
 """"     _/ ______ __  ____   ____ |__|     """(___:)
  ||      \   __|  |  \/    \ / ___\|  |      || """"
  ||       |  | |  |  |   |  / /_/  |  |      ||  ||
  ))       |__| |____/|___|  \___  /|__|      ((  ))
 //                        \/_____/            \\//
((                                              )/
 \\                                            //
  ))                                          ((
  ||                                          ||
EOF
    echo "  ||                              $FUNGI_VERSION"
    printf "${CY}================= ${CU}${CG}%s${CZ}${CY} =================${CZ}\n" "$1"
}

_set_colors() {
  CG=""
  CY=""
  Cy=""
  CR=""
  CZ=""
  CU=""

  COLORS=$(tput colors 2> /dev/null)
  if [ $? = 0 ] && [ $COLORS -gt 2 ]; then
    CG="$( tput setaf 2 )$( tput bold )"
    CY="$( tput setaf 3 )$( tput bold )"
    Cy="$( tput setaf 3 )$( tput dim )"
    CR="$( tput setaf 1 )$( tput bold )"
    CZ="$( tput sgr0 )"
    CU="$( tput smul )" #underline
  fi
}


_warning() {
  printf "${CY}%s${CZ}\n" \
   "$1" >&2
}


_check_threads() {
    while [[ ${#PIDS[@]} -gt $threads ]]; do
        keep_pids=()
        for pid in ${PIDS[@]}; do
            kill -0 "$pid" >/dev/null 2>&1 && keep_pids+=( $pid ) || true
        done
        PIDS=()
        for pid in ${keep_pids[@]}; do
            PIDS+=($pid)
        done
        sleep 1
    done
}

_wait_threads() {
    for pid in ${PIDS[@]}; do
        wait $pid
    done
}

_which() {
    which "$1"
    if [[ $? -ne 0 ]]; then
        _error "No such executable in path: $1"
    fi
}


FUSIONANALYZER_HOME="$SCRIPTPATH"

_set_colors
