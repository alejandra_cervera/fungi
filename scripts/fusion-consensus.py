#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  1 11:35:00 2021

@author: cerverat

"""

import argparse
import csv
import logging
import os
import pandas as pd
import sys
import textwrap


def get_options():
    parser = argparse.ArgumentParser(
        prog=sys.argv[0],
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent(
            """\
-Finds consensus breakpoint for fusions with same gene partners in the same sample
-Takes outputs from fusion-analyzer (final.candidates.csv)
-Filters results using a fusion_list to retain only selected fusions
-Useful for combining fusion-analyzer and fusion-visualizer results (some preformatting needed).
-Reports the best candidate breakpoint based on the number of supporting methods and reads,
and also reports best candidate breakpoint based on number of samples with reported breakpoint.

Argument description:

fusion-analyzer:
    final.candidates.csv output file from fusion-analyzer
input:
    File with columns
        -FusionName (gene1--gene2)
        -Sample (Sample id)
        -Methods (single method or comma separated list of methods)
        -Encompassing reads (same as JunctionReadCount)
        -Split reads (same as SpanningFragCount)
        -bp_coordinates (chr:leftbreakpoint>chr:rightbreakpoint)
    Column names does not matter, but order does.
    To use fusioninspector output the Sample and Methods column needs to be added,
    and LeftBreakpoint and RightBreakpoint columns need to be transformed into bp_coordinates column
fusion_list:
    File with column (additional columns will be ignored)
        -FusionName (gene1--gene2)
    Column name does not matter but a header is needed
    If this fusion_list is provided fusions not in the list will be discarded
output:
    Filename for consensus fusion file.

    """
        ),
    )
    parser.add_argument("--input", "-i", help="file with fusions")
    parser.add_argument(
        "--fusion_analyzer",
        "--fusion-analyzer",
        help="final.candidates.csv output file from fusion-analyzer",
    )
    parser.add_argument(
        "--fusion_list", "--fusion-list", help="list of fusions to keep"
    )
    parser.add_argument(
        "--output",
        "-o",
        help="output folder for consensus results",
    )
    parser.add_argument(
        "--debug", action="store_true", help="More verbose messages", default=False
    )
    options = parser.parse_args()
    if options.debug:
        logging.basicConfig(level=logging.DEBUG, format="%(levelname)s: %(message)s")
    else:
        logging.basicConfig(level=logging.INFO, format="%(levelname)s: %(message)s")

    if (options.fusion_analyzer is None) & (options.input is None):
        parser.print_help()
        logging.error("Both fusion_analyzer and input are missing!")
        sys.exit(1)

    return options


def get_cols(fusions):
    logging.debug("Get columns")
    data = pd.read_csv(fusions, sep="\t", dtype="str", header=0)
    df = pd.DataFrame(data)
    df["Encompassing reads"] = pd.to_numeric(df["Encompassing reads"], errors="coerce")
    df["Split reads"] = pd.to_numeric(df["Split reads"], errors="coerce")
    # get sample name from source field
    df["Sample"] = df["Source"].str.rsplit("_", n=1).str.get(0)
    df1 = df.loc[
        :,
        [
            "FusionName",
            "Sample",
            "Methods",
            "Encompassing reads",
            "Split reads",
            "bp_coordinates",
            "Sample_occurrence",
        ],
    ]
    return df1


def prep_input(fusion_input):
    logging.debug("Prepare input")
    data = pd.read_csv(fusion_input, sep="\t", dtype="str", header=0)
    df_in = pd.DataFrame(data)
    df_in[df_in.columns[3]] = df_in[df_in.columns[3]].astype(int)
    df_in[df_in.columns[4]] = df_in[df_in.columns[4]].astype(int)
    df_in.columns = [
        "FusionName",
        "Sample",
        "Methods",
        "Encompassing reads",
        "Split reads",
        "bp_coordinates",
    ]
    df_in["Sample_occurrence"] = df_in["Sample"]
    return df_in


def combine(fusion_analyzer, fusion_input):
    logging.debug("Combine")
    df_fa = get_cols(fusion_analyzer)
    df_in = prep_input(fusion_input)
    df = pd.concat([df_fa, df_in])
    return df


def filterfusions(data, fusion_list):
    logging.debug("Filter Fusions")
    df = pd.read_csv(fusion_list, sep="\t", header=0)
    myFusions = df.loc[:, "FusionName"]
    df1 = data[data.FusionName.isin(myFusions)]
    return df1


def count(data, outfile):
    logging.debug("Count")
    # group by bp_coordinates for each sample and sum the reads and list the methods
    df = data.groupby(["FusionName", "bp_coordinates", "Sample"]).agg(
        {
            "Encompassing reads": "sum",
            "Split reads": "sum",
            "Methods": ",".join,
            "Sample_occurrence": ",".join,
        }
    )

    # remove duplicates from methods list
    df["Methods"] = df["Methods"].apply(
        lambda x: ",".join(sorted(set([y.strip() for y in x.split(",")])))
    )
    df["MethodsCount"] = df["Methods"].apply(lambda x: len(x.split(",")))
    df["Sample_occurrence"] = df["Sample_occurrence"].apply(
        lambda x: ",".join(sorted(set([y.strip() for y in x.split(",")])))
    )
    df["SampleCount"] = df["Sample_occurrence"].apply(lambda x: len(x.split(",")))
    df.to_csv(outfile, sep="\t")
    return df.reset_index()


def consensus(new_df):
    logging.debug("Run Consensus")
    best = {}
    for i, row in new_df.iterrows():
        f = row["FusionName"]
        bp = row["bp_coordinates"]
        s = row["Sample"]
        r1 = row["Encompassing reads"]
        r2 = row["Split reads"]
        m = row["MethodsCount"]
        ml = row["Methods"]
        soc = row["Sample_occurrence"]
        sct = row["SampleCount"]
        new_key = f + "|" + s
        # print(f,m)

        if not new_key in best:
            # add fusion to dict
            best[new_key] = [m, r1, r2, bp, ml, "-", soc, sct, bp, "-", soc]
            continue

        # fusion already in array
        new_best = [
            m,
            r1,
            r2,
            bp,
            ml,
            "-",
            soc,
            sct,
            best[new_key][8],
            best[new_key][9],
            best[new_key][10],
        ]
        # update bp if more methods support it
        if m > best[new_key][0]:
            best[new_key] = new_best
        # methods tie
        elif m == best[new_key][0]:
            # update bp if more junction reads support it
            if r1 > best[new_key][1]:
                best[new_key] = new_best
            # junction reads tie
            elif r1 == best[new_key][1]:
                # update bp if more spanning reads support it
                if r2 > best[new_key][2]:
                    best[new_key] = new_best
                # no clear better bp
                # spanning read tie
                elif r2 == best[new_key][2]:
                    best[new_key][5] = bp

        if sct > best[new_key][7]:
            best[new_key][8] = bp
            best[new_key][10] = soc
        elif sct == best[new_key][7]:
            best[new_key][9] = bp

    return best


def write_res(best, outfile):
    logging.debug("Write output")
    consensus = open(outfile, "w")
    writer = csv.writer(consensus, delimiter="\t")
    writer.writerow(
        [
            "FusionName",
            "Sample",
            "best_bp",
            "Methods_count",
            "Methods_list",
            "junction_reads",
            "spanning_reads",
            "same_score_bp",
            "Sample_occurrence_method_score",
            "SampleCount",
            "best_by_sample_count",
            "same_score_sample_count",
            "Sample_occurence_sample_score",
        ]
    )
    for key in sorted(best):
        s = key.split("|")[1]
        bp = best[key][3]
        fn = key.split("|")[0]
        row = [
            fn,
            s,
            bp,
            best[key][0],
            best[key][4],
            best[key][1],
            best[key][2],
            best[key][5],
            best[key][6],
            best[key][7],
            best[key][8],
            best[key][9],
            best[key][10],
        ]
        # logging.debug(row)
        writer.writerow(row)
    consensus.close()


if __name__ == "__main__":
    opts = get_options()

    if os.path.exists(opts.output):
        logging.warning("Folder {} already exists".format(opts.output))
    else:
        os.makedirs(opts.output)

    if (opts.fusion_analyzer is not None) & (opts.input is not None):
        fusions = combine(opts.fusion_analyzer, opts.input)
    elif opts.fusion_analyzer is not None:
        fusions = get_cols(opts.fusion_analyzer)
    elif opts.input is not None:
        fusions = prep_input(opts.input)
    else:
        logging.error("Both fusion-analyzer and input are missing")
        sys.exit(1)

    if opts.fusion_list is not None:
        filtered = filterfusions(fusions, opts.fusion_list)
    else:
        filtered = fusions

    df = count(filtered, os.path.join(opts.output, "combined_fusions_report.csv"))
    consensus_df = consensus(df)
    write_res(consensus_df, os.path.join(opts.output, "bp_consensus_report.csv"))
