#!/usr/bin/env python3

import sys
import argparse
import csv
from pprint import pprint


def get_options():
    parser = argparse.ArgumentParser(
        description="Import files for Fusion Analyzer"
    )
    parser.add_argument('--input1')
    parser.add_argument('--input2')
    parser.add_argument('--output')

    options=parser.parse_args()
    return options


def join_inputs(input1, input2):

    with open(input1, 'rt') as f1:
        with open(input2, 'rt') as f2:
            reader1 = csv.DictReader(f1, dialect='excel-tab')
            reader2 = csv.DictReader(f2, dialect='excel-tab')

            data1 = [row for row in reader1]
            data2 = [row for row in reader2]

            joined = []

            for row1 in data1:
                for row2 in data2:
                    #it is enough to check gene ids are the same
                    if all((
                        row1['gene_id1'] == row2['gene1_ID'],
                        row1['gene_id2'] == row2['gene2_ID'],
                    )):
                        row1.update(row2)
                        joined.append(row1)
    return joined


def reformat(data, filename):
    with open(filename, 'wt') as fp:
        writer = csv.DictWriter(
            fp,
            fieldnames="gene_name1 gene_name2 ensembl_gene_id1 ensembl_gene_id2 gene1_coord gene2_coord bp junction_reads spanning_reads annotations".split(),
            dialect="excel-tab",
            quoting=csv.QUOTE_NONE
        )
        writer.writeheader()
        for row in data:
            writer.writerow({
                'gene_name1': row['gene_name1'],
                'gene_name2': row['gene_name2'],
                'ensembl_gene_id1': row['gene_id1'],
                'ensembl_gene_id2': row['gene_id2'],
                'gene1_coord': "{}:{}:{}".format(
                    row['start_position1'],
                    row['end_position1'],
                    row['gene_strand1']
                ),
                'gene2_coord': "{}:{}:{}".format(
                    row['start_position2'],
                    row['end_position2'],
                    row['gene_strand2']
                 ),
                'bp': "chr{}:{}-chr{}:{}".format(
                    row['chr1'],
                    row['bp1'],
                    row['chr2'],
                    row['bp2']
                ),
                'junction_reads': row['junction_reads'],
                'spanning_reads': row['spanning_reads'],
                'annotations': row['Fusion_description']
            })


if __name__ == "__main__":
    opts = get_options()
    joined = join_inputs(opts.input1, opts.input2)
    reformat(joined, opts.output)

