#!/bin/bash

set -e
set -u

#fusioncatcher home folder
fc=$1

#fusioncatcher db for annotation
ensembl=$2

#the file with the fusions
in_raw=$3

#where the annotated files should go
annot_dir="$4"

#where the filtered files should go
out_dir="$5"

#sample name and tool
name=$6

#terms for filtering
dbs=$( echo ${7} | tr -d '"' | tr ',' '|' )

#log file to put the discarded fusions
log=$8

#debug option
debug=$9
if [[ "$debug" = true ]]; then
  set -x
fi

tmp_folder="${annot_dir}/${name}_tmp"

in="${tmp_folder}/in"
out="${out_dir}/${name}.csv"

python -c "
import csv, sys

with open(sys.argv[1], 'rt') as fr:
    with open(sys.argv[2], 'wt') as fw:
        writer = csv.writer(fw, dialect='excel-tab', quoting=csv.QUOTE_NONE)
        writer.writerow('gene1_ID gene2_ID spanning gene1_name gene2_name'.split())
        reader = csv.DictReader(fr, dialect='excel-tab')
        for row in reader:
            writer.writerow([
                row['ensembl_gene_id1'],
                row['ensembl_gene_id2'],
                row['spanning_reads'],
                row['gene_name1'],
                row['gene_name2'],
            ])
" \
  "$in_raw" \
  "$in"


python ${fc}label_fusion_genes.py --input $in --label no_protein --filter_genes ${ensembl}genes_with_no_proteins.txt --output_fusion_genes ${tmp_folder}/no_prot
python ${fc}label_fusion_genes.py --input ${tmp_folder}/no_prot --label paralogs --filter_gene_pairs ${ensembl}paralogs.txt --output_fusion_genes ${tmp_folder}/paralog
python ${fc}label_fusion_genes.py --input ${tmp_folder}/paralog --label adjacent --filter_gene_pairs ${ensembl}adjacent_genes.txt --output_fusion_genes ${tmp_folder}/adjacent
python ${fc}label_fusion_genes.py --input ${tmp_folder}/adjacent --label ensembl_fully_overlapping --filter_gene_pairs ${ensembl}ensembl_fully_overlapping_genes.txt --output_fusion_genes ${tmp_folder}/overlapping
python ${fc}label_fusion_genes.py --input ${tmp_folder}/overlapping --label ensembl_partially_overlapping --filter_gene_pairs ${ensembl}ensembl_partially_overlapping_genes.txt --output_fusion_genes ${tmp_folder}/partially
python ${fc}label_fusion_genes.py --input ${tmp_folder}/partially --label ensembl_same_strand_overlapping --filter_gene_pairs ${ensembl}ensembl_same_strand_overlapping_genes.txt --output_fusion_genes ${tmp_folder}/same_strand
python ${fc}label_fusion_genes.py --input ${tmp_folder}/same_strand --label refseq_fully_overlapping --filter_gene_pairs ${ensembl}refseq_fully_overlapping_genes.txt --output_fusion_genes ${tmp_folder}/refseq
python ${fc}label_fusion_genes.py --input ${tmp_folder}/refseq --label refseq_partially_overlapping --filter_gene_pairs ${ensembl}refseq_partially_overlapping_genes.txt --output_fusion_genes ${tmp_folder}/refseq_partially
python ${fc}label_fusion_genes.py --input ${tmp_folder}/refseq_partially --label refseq_same_strand_overlapping --filter_gene_pairs ${ensembl}refseq_same_strand_overlapping_genes.txt --output_fusion_genes ${tmp_folder}/refseq_same_strand
python ${fc}label_fusion_genes.py --input ${tmp_folder}/refseq_same_strand --label duplicates --filter_gene_pairs ${ensembl}dgd.txt --output_fusion_genes ${tmp_folder}/duplicates
python ${fc}label_fusion_genes.py --input ${tmp_folder}/duplicates --label tcga --filter_gene_pairs ${ensembl}tcga.txt --output_fusion_genes ${tmp_folder}/tcga
python ${fc}label_fusion_genes.py --input ${tmp_folder}/tcga --label bodymap --filter_gene_pairs ${ensembl}bodymap2.txt --output_fusion_genes ${tmp_folder}/bodymap
python ${fc}label_fusion_genes.py --input ${tmp_folder}/bodymap --label metazoa --filter_gene_pairs ${ensembl}metazoa.txt --output_fusion_genes ${tmp_folder}/metazoa
python ${fc}label_fusion_genes.py --input ${tmp_folder}/metazoa --label cell_lines --filter_gene_pairs ${ensembl}celllines.txt --output_fusion_genes ${tmp_folder}/cell_lines
python ${fc}label_fusion_genes.py --input ${tmp_folder}/cell_lines --label ambiguous --filter_gene_pairs ${ensembl}all_ambiguous_genes.txt --output_fusion_genes ${tmp_folder}/ambiguous
python ${fc}label_fusion_genes.py --input ${tmp_folder}/ambiguous --label gencode_fully_overlapping --filter_gene_pairs ${ensembl}genecode_fully_overlapping_genes.txt --output_fusion_genes ${tmp_folder}/genecode
python ${fc}label_fusion_genes.py --input ${tmp_folder}/genecode --label genecode_partially_overlapping --filter_gene_pairs ${ensembl}genecode_partially_overlapping_genes.txt --output_fusion_genes ${tmp_folder}/genecode_partially
python ${fc}label_fusion_genes.py --input ${tmp_folder}/genecode_partially --label genecode_same_strand_overlapping --filter_gene_pairs ${ensembl}genecode_same_strand_overlapping_genes.txt --output_fusion_genes ${tmp_folder}/genecode_same_strand
python ${fc}label_fusion_genes.py --input ${tmp_folder}/genecode_same_strand --label prostates --filter_gene_pairs ${ensembl}prostates.txt --output_fusion_genes ${tmp_folder}/prostates
python ${fc}label_fusion_genes.py --input ${tmp_folder}/prostates --label non_tumor_cells --filter_gene_pairs ${ensembl}non-tumor_cells.txt --output_fusion_genes ${tmp_folder}/non_tumor
python ${fc}label_fusion_genes.py --input ${tmp_folder}/non_tumor --label hpa --filter_gene_pairs ${ensembl}hpa.txt --output ${tmp_folder}/hpa
python ${fc}label_fusion_genes.py --input ${tmp_folder}/hpa --label distance200kbp --filter_gene_pairs ${ensembl}exons.txt --output ${tmp_folder}/distance200kbp
python ${fc}label_fusion_genes.py --input ${tmp_folder}/distance200kbp --label gtex --filter_gene_pairs ${ensembl}gtex.txt --output_fusion_genes ${tmp_folder}/gtex
python ${fc}label_fusion_genes.py --input ${tmp_folder}/gtex --label non_cancer_tissues --filter_gene_pairs ${ensembl}non-cancer_tissues.txt --output_fusion_genes ${tmp_folder}/non_cancer_tissues
python ${fc}label_fusion_genes.py --input ${tmp_folder}/non_cancer_tissues --label hla --filter_gene_pairs ${ensembl}hla.txt --output_fusion_genes ${tmp_folder}/hla
python ${fc}label_fusion_genes.py --input ${tmp_folder}/hla --label 1000genomes --filter_gene_pairs ${ensembl}1000genomes.txt --output_fusion_genes ${tmp_folder}/1000genomes
python ${fc}label_fusion_genes.py --input ${tmp_folder}/1000genomes --label 18cancers --filter_gene_pairs ${ensembl}18cancers.txt --output_fusion_genes ${tmp_folder}/18cancers
python ${fc}label_fusion_genes.py --input ${tmp_folder}/18cancers --label gliomas --filter_gene_pairs ${ensembl}gliomas.txt --output_fusion_genes ${tmp_folder}/gliomas
python ${fc}label_fusion_genes.py --input ${tmp_folder}/gliomas --label chimerdb3kb --filter_gene_pairs ${ensembl}chimerdb3kb.txt --output_fusion_genes ${tmp_folder}/chimerdb3kb
python ${fc}label_fusion_genes.py --input ${tmp_folder}/chimerdb3kb --label oesophagus --filter_gene_pairs ${ensembl}oesophagus.txt --output_fusion_genes ${tmp_folder}/oesophagus
python ${fc}label_fusion_genes.py --input ${tmp_folder}/oesophagus --label pancreases --filter_gene_pairs ${ensembl}pancreases.txt --output_fusion_genes ${tmp_folder}/pancreases
python ${fc}label_fusion_genes.py --input ${tmp_folder}/pancreases --label banned --filter_gene_pairs ${ensembl}banned.txt --output_fusion_genes ${tmp_folder}/banned
python ${fc}label_fusion_genes.py --input ${tmp_folder}/banned --label similar_reads --filter_gene_pairs ${ensembl}list_candidates_ambiguous_homologous_genes.txt --output_fusion_genes ${tmp_folder}/similar_reads
python ${fc}label_fusion_genes.py --input ${tmp_folder}/similar_reads --label pseudogene --filter_gene_pairs ${ensembl}pseudogenes.txt --output ${tmp_folder}/pseudogene
python ${fc}label_fusion_genes.py --input ${tmp_folder}/pseudogene --label rrna --filter_gene_pairs ${ensembl}rrnas.txt --output ${tmp_folder}/rrnas
python ${fc}label_fusion_genes.py --input ${tmp_folder}/rrnas --label trna --filter_gene_pairs ${ensembl}trnas.txt --output ${tmp_folder}/trnas
python ${fc}label_fusion_genes.py --input ${tmp_folder}/trnas --label mirna --filter_gene_pairs ${ensembl}mirnas.txt --output ${tmp_folder}/mirnas
python ${fc}label_fusion_genes.py --input ${tmp_folder}/mirnas --label lincrna --filter_gene_pairs ${ensembl}lincrna.txt --output ${tmp_folder}/lincrna
python ${fc}label_fusion_genes.py --input ${tmp_folder}/lincrna --label mt --filter_gene_pairs ${ensembl}mt.txt --output ${tmp_folder}/mt
python ${fc}label_fusion_genes.py --input ${tmp_folder}/mt --label snornas --filter_gene_pairs ${ensembl}snornas.txt --output ${tmp_folder}/snornas
python ${fc}label_fusion_genes.py --input ${tmp_folder}/snornas --label snrnas --filter_gene_pairs ${ensembl}snrnas.txt --output ${tmp_folder}/snrnas
python ${fc}label_fusion_genes.py --input ${tmp_folder}/snrnas --label yrnas --filter_gene_pairs ${ensembl}yrnas.txt --output ${tmp_folder}/yrnas
python ${fc}label_fusion_genes.py --input ${tmp_folder}/yrnas --label 7skrnas --filter_gene_pairs ${ensembl}7skrnas.txt --output ${tmp_folder}/7skrnas
python ${fc}label_fusion_genes.py --input ${tmp_folder}/7skrnas --label antisense --filter_gene_pairs ${ensembl}antisenses.txt --output ${tmp_folder}/antisense
python ${fc}label_fusion_genes.py --input ${tmp_folder}/antisense --label pairs_pseudogenes --filter_gene_pairs ${ensembl}pairs_pseudogenes.txt --output ${tmp_folder}/pairs_pseudogenes
python ${fc}label_fusion_genes.py --input ${tmp_folder}/pairs_pseudogenes --label ribosomal_protein --filter_gene_pairs ${ensembl}ribosomal_proteins.txt --output ${tmp_folder}/ribosomal
python ${fc}label_fusion_genes.py --input ${tmp_folder}/ribosomal --label oncogene --filter_gene_pairs ${ensembl}oncogenes.txt --output ${tmp_folder}/oncogenes
python ${fc}label_fusion_genes.py --input ${tmp_folder}/oncogenes --label known --filter_gene_pairs ${ensembl}known.txt --output ${tmp_folder}/known
python ${fc}label_fusion_genes.py --input ${tmp_folder}/known --label cosmic --filter_gene_pairs ${ensembl}cosmic.txt --output ${tmp_folder}/cosmic
python ${fc}label_fusion_genes.py --input ${tmp_folder}/cosmic --label chimerdb2 --filter_gene_pairs ${ensembl}chimerdb2.txt --output ${tmp_folder}/chimerdb2
python ${fc}label_fusion_genes.py --input ${tmp_folder}/chimerdb2 --label conjoing --filter_gene_pairs ${ensembl}conjoing.txt --output ${tmp_folder}/conjoining
python ${fc}label_fusion_genes.py --input ${tmp_folder}/conjoining --label cgp --filter_gene_pairs ${ensembl}cgp.txt --output ${tmp_folder}/cgp
python ${fc}label_fusion_genes.py --input ${tmp_folder}/cgp --label ticdb --filter_gene_pairs ${ensembl}ticdb.txt --output ${tmp_folder}/ticdb
python ${fc}label_fusion_genes.py --input ${tmp_folder}/ticdb --label rp11 --filter_gene_pairs ${ensembl}rp11.txt --output ${tmp_folder}/rp11
python ${fc}label_fusion_genes.py --input ${tmp_folder}/rp11 --label cta --filter_gene_pairs ${ensembl}cta.txt --output ${tmp_folder}/cta
python ${fc}label_fusion_genes.py --input ${tmp_folder}/cta --label ctb --filter_gene_pairs ${ensembl}ctb.txt --output ${tmp_folder}/ctb
python ${fc}label_fusion_genes.py --input ${tmp_folder}/ctb --label ctc --filter_gene_pairs ${ensembl}ctc.txt --output ${tmp_folder}/ctc
python ${fc}label_fusion_genes.py --input ${tmp_folder}/ctc --label ctd --filter_gene_pairs ${ensembl}ctd.txt --output ${tmp_folder}/ctd
python ${fc}label_fusion_genes.py --input ${tmp_folder}/ctd --label rp_gene --filter_gene_pairs ${ensembl}rp.txt --output ${tmp_folder}/rp
python ${fc}label_fusion_genes.py --input ${tmp_folder}/rp --label healthy --filter_gene_pairs ${ensembl}healthy.txt --output ${tmp_folder}/healthy
python ${fc}label_fusion_genes.py --input ${tmp_folder}/healthy --label cacg --filter_gene_pairs ${ensembl}cacg.txt --output ${tmp_folder}/cacg
python ${fc}label_fusion_genes.py --input ${tmp_folder}/cacg --label ucsc_fully_overlapping --filter_gene_pairs ${ensembl}ucsc_fully_overlapping_genes.txt --output_fusion_genes ${tmp_folder}/ucsc
python ${fc}label_fusion_genes.py --input ${tmp_folder}/ucsc --label ucsc_partially_overlapping --filter_gene_pairs ${ensembl}ucsc_partially_overlapping_genes.txt --output_fusion_genes ${tmp_folder}/ucsc_partially
python ${fc}label_fusion_genes.py --input ${tmp_folder}/ucsc_partially --label ucsc_same_strand_overlapping --filter_gene_pairs ${ensembl}ucsc_same_strand_overlapping_genes.txt --output_fusion_genes ${tmp_folder}/ucsc_same_strand
set +e
tail -n +2 ${tmp_folder}/ucsc_same_strand | sort -u > ${tmp_folder}/prefiltered

#Save in out kept fusions
head -n1 ${tmp_folder}/ucsc_same_strand > $out
grep -Ev "$dbs" ${tmp_folder}/prefiltered >> $out

#Save in log removed fusions
head -n1 ${tmp_folder}/ucsc_same_strand > $log
grep -E "$dbs" ${tmp_folder}/prefiltered >> $log

echo "Exit DB annotation"
