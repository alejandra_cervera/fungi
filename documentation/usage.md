# Usage

[TOC]

# FusionCaller

FusionCaller eases calling fusions with any of the six supported algorithms: [STAR-Fusion](https://github.com/STAR-Fusion/STAR-Fusion/wiki), [FusionCatcher](https://github.com/ndaniel/fusioncatcher), [Arriba](https://arriba.readthedocs.io/en/latest/), [ChimeraScan](https://github.com/biobuilds/chimerascan), [EricScript](https://sourceforge.net/projects/ericscript/files/) or [SoapFuse](https://sourceforge.net/p/soapfuse/wiki/Home/).

Use your own data or use the reads.fq.gz and mates.fq.gz in resources/ (if not there run resources/download.sh).

Run without parameters to see usage info:
```
fungi-fusion-caller
```


An example run for each tool, with switches:
`-t toolname`
`-th number_of_threads`
`-o out_dir`
`-r reads`
`-m mates`
`-s sample_name`

```
fungi-fusion-caller -c myConfig.txt -r reads.fq.gz -m mates.fq.gz -o out_Arriba -t arriba -s sample1 -th 8
fungi-fusion-caller -c myConfig.txt -r reads.fq.gz -m mates.fq.gz -o out_CS -t chimerascan -s sample1 -th 8
fungi-fusion-caller -c myConfig.txt -r reads.fq.gz -m mates.fq.gz -o out_DF -t defuse -s sample1 -th 8
fungi-fusion-caller -c myConfig.txt -r reads.fq.gz -m mates.fq.gz -o out_ES -t ericscript -s sample1 -th 8
fungi-fusion-caller -c myConfig.txt -r reads.fq.gz -m mates.fq.gz -o out_FC -t fusioncatcher -s sample1 -th 8
fungi-fusion-caller -c myConfig.txt -r reads.fq.gz -m mates.fq.gz -o out_SF -t soapfuse -s sample1 -th 8 -l 102
fungi-fusion-caller -c myConfig.txt -r reads.fq.gz -m mates.fq.gz -o out_STAR -t starfusion -s sample1 -th 8
```

The output folder contains all the files produced by each tool (check the documentation of the tool used for details).

Three additional files are created to help facilitate integration with downstream steps:

- final (copy of main results in each output folder)
- .sample (has the sample id)
- .tool (has the name of the algorithm used to identify the fusion genes)



# FusionAnalyzer

FusionAnalyzer integrates the fusions called by the different tools, annotates and scores all of them. Filtering is done based on number of supporting reads, Ensembl and known fusion gene database annotations.

Create an input file with the paths to the final fusion lists that you
want to analyze. The file most contain 3 tab separated colums: Sample,
Tool, File.

Check the example [fa_input.csv](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/fa_input.csv).

If you didn't use fusion-caller to call the fusions then you need to
specify the file with the final fusion list. If you didn't use any of
the supported fusion callers then provide the fusions in the following
format (tab separated): gene1\_name, gene2\_name, chr1, bp1,
gene\_strand1, chr2, bp2, gene\_strand2, junction\_reads,
spanning\_reads. Example:
[generic\_fusions.csv](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/generic_fusions.csv).

```
# run without parameters to see usage info
fungi-fusion-analyzer

# annotate fusions with Ensembl ids and fusion genes databases & discard fusions
fungi-fusion-analyzer -c myConfig.txt -i input.csv annotate -o out1 --filter-ensembl invalid_gene_name,invalid_gene_id,mismatch_with_ensembl --filter-db banned,healthy --filter-min-count 3

# score fusions with Pegasus and Oncofuse; this step can only be run on the same output folder as annotate step without specifying -i
fungi-fusion-analyzer -c myConfig.txt scoring -o out1 --tissue AVG --clearDB

# annotate & score
fungi-fusion-analyzer -c myConfig.txt -i input.csv annotate scoring -o test1 --tissue AVG --clearDB --filter-ensembl invalid_gene_name,invalid_gene_id --filter-db banned,healthy
```

`annotate` and `scoring` can be done together in the same command or
stepwise. If `annotate` is run first, then the output folder for
`scoring` has to be the same as the `annotate` step, and the input
should not be specified.

Filtering is performed in the `annotate` step; there are 3 filtering
options:

-   `--filter-min-count`:    discards fusions with number of junction
    reads below this parameter.
-   `--filter-ensembl`:    options are: invalid\_gene\_id,
    invalid\_gene\_name, mismatch\_with\_ensembl, same\_gene,
    homologous, no\_GO\_term. If we were not able to match the gene
    name or the gene id to the current ensembl annotation, then the
    fusion will be annotated with invalid\_gene\_\[id\|name\] and those
    fusions will fail scoring, so it is recommended to discard them
    (*invalid\_gene* works for both). A fusion will be annotated with
    *mismatch\_with\_ensembl* if the breakpoint is outside the gene
    region for either gene; *same\_gene,homologous* are used to discard
    fusions with both genes have the same name or are homologous;
    *no\_GO\_term* discards fusions where neither gene has a function in
    GO.
-   `--filter-db`:    discard any of the fusions annotated by
    FusionCatcher with the following terms: 1000genomes, 18cancers,
    adjacent, banned, bodymap, cacg, cell\_lines, cgp, chimerdb2,
    chimerdb3kb, conjoing, cosmic, cta, ctb, ctc, ctd, duplicates,
    ensembl\_fully\_overlapping, ensembl\_partially\_overlapping,
    ensembl\_same\_strand\_overlapping, fragments, gliomas, gtex,
    healthy, hpa, known, metazoa, mirna, mt, non\_cancer\_tissues,
    non\_tumor\_cells, no\_protein, oesophagus, overlapping,
    pairs\_pseudogenes, pancreases, paralogs, prostates,
    refseq\_fully\_overlapping, refseq\_partially\_overlapping,
    refseq\_same\_strand\_overlapping, rp11, rp, similar\_reads,
    similar\_symbols, tcga, ucsc\_fully\_overlapping,
    ucsc\_partially\_overlapping, ucsc\_same\_strand\_overlapping,yrna.

Oncofuse and Pegasus parameters:

-   `--tisue`:    EPI\|HEM\|MES\|AVG use AVG if tumor tissue is not
    epithelial, hematological or mesencnymal.
-   `--clearDB`:    with this parameter you can restart Pegasus\'
    database. Leave it out to keep fusons in the database and get the
    list of samples with matching fusion on different runs.

The outputs:

-   `out_dir/scored/final.candidates.csv`:    this list is the main
    output, it includes the integrated list of fusions from all samples
    and tools, with the annotations from Ensembl and fusion gene
    databases and the scoring from Pegasus and Oncofuse and the most
    relevant columns from each tool.
-   `out_dir/annotated`:    the fusions with ensembl ids, gene
    coordinates, and annotations from Ensembl and gene fusion databases.
-   `out_dir/logs/`:    information of discarded fusions from each of
    the filtering options.



# FusionVisualizer

There are two modes for visualizing fusions:

- create a virtual reference based on the exact fusion with the
    breakpoints detected by the fusion callers
- or use
    [FusionInspector](https://github.com/FusionInspector/FusionInspector/wiki)
    to find the fusion based in the gene names only and it recreates the
    fusion using Trinity and maps to it.

Use the `--use-bp` parameter to choose between each mode. The
`out_dir/scored/final.candidates.csv` can be used directly as input for
`fungi-fusion-visualizer`; other fusion lists with the following:
ensembl\_id\_gene1, ensembl\_id\_gene2, bp\_coordinates (the column
names are provided as parameters). Check the example
[fusions\_bp.csv](../resources/fusions_bp.csv).

For using
[FusionInspector](https://github.com/FusionInspector/FusionInspector/wiki),
the fusion names should be in the first column in the format
gene1\--gene2 and all other columns will be ignored if present. Check
the example [fusion\_names.csv](../resources/fusion_names.csv).

```
#run without parameters to see usage info
fungi-fusion-visualizer

# run WITHOUT breakpoint information; input list can be just fusion names
fungi-fusion-visualizer -c myConfig.txt -i test1/scored/final.candidates.list -r $reads -m $mates --sample sample1 -o out_vis

# run WITH breakpoint information;  input list needs ensembl ids and breakpoint coordinates; column names are specified as paramter if not using default names
fungi-fusion-visualizer -c myConfig.txt -i test1/scored/final.candidates.list -r $reads -m $mates --sample sample1 -o out_vis --use-bp
```

The outputs:

-   FusionVisualizer without breakpoint:
    check [FusionInspector](https://github.com/FusionInspector/FusionInspector/wiki)
    for a detailed description of the output files. Several files
    contain the list of fusions detected in the sample provided with
    different levels of annotation. Additionally a reference file with
    the fusions and alignment files can be visualized with genome
    browsers like
    [IGV](http://software.broadinstitute.org/software/igv/).
-   FusionVisualizer with breakpoint information: two reference files are
    created, one with the genes side by side (virtual\_ref1.fa) and one
    with the genes merged at the breakpoint (virtual\_ref2.fa); the
    alignment to both virtual references can be visualized with genome
    browsers like
    [IGV](http://software.broadinstitute.org/software/igv/).
    [Example](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/fv_bp_igv_screenshot.pdf) of a recreated fusion.


# FusionConsensus

Reports the majority breakpoint for each fusion when more than one was identified by the different (or the same) fusion calling algorithms.

The consensus breakpoint is selected first by the number of methods identifying the breakpoint, when there is a tie, the junction reads and the split reads are used to decide the best candidates. Junction and split reads are summed accross methods. The reported reads therefore do not reflect the reads supporting a fusion in a given sample, but the sum of the reports for each method. Additionally, the best breakpoint in terms of number of samples, is also includded in the report.

FusionConsensus also combines output from FusionAnalyzer and from FusionVisualizer (when used with FusionInspector) and let's you retain only the fusions of interest by providing a fusion list. 


```
#run without parameters to see usage info
fungi-fusion-consensus

# Example run
fungi-fusion-consensus
    --input "fcs_input.csv" \
    --fusion_analyzer "fcs_fusion_analyzer.csv" \
    --fusion_list "$fcs_fusion_list.csv"
    -o "fca_consensus.csv"
```

At least one of the following inputs is required, but both can be supplied:

The --input parameter requires the following columns: FusionName, Sample, Methods, Junction reads, split reads, bp_coordinates. Column names do not matter, but order does. This is the preferred way to input FusionInspector results once Sample and Methods column have been added and additional columns removed.

The --fusion_analyzer parameter requires (at least) the following columns: FusionName, Sample, Methods, Encompassing reads, Split reads, bp_coordinates. In this case, column names matter, order does not. These column names match FusionAnalyzer output file, additional columns can be in the file and will be ignored.

The --fusion_list input is used to subset the fusions to be analyzed (fusions included in the inputs but not in the fusion_list will be ignored).

The outputs:

- combined_fusions_report.csv]: the combined results from --input and --fusion_analyzer input files.
- bp_consensus_report.csv: the breakpoint consensus report


# Example Data Files

Both FusionCaller and FusionVisualizer require input `reads` and `mates`
and they need to be run on each sample set separately. FusionAnalyzer only
needs to be run once with the whole list of fusion calls from all methods and sample sets.

## FusionCaller

Inputs:

- RNA-seq [reads](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/reads.fq.gz) and
  [mates](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/mates.fq.gz)

Outputs:

- The output fusion call folder for each tool, check their documentation for details:
  [STAR-Fusion](https://github.com/STAR-Fusion/STAR-Fusion/wiki),
  [FusionCatcher](https://github.com/ndaniel/fusioncatcher),
  [Arriba](https://arriba.readthedocs.io/en/latest/),
  [ChimeraScan](https://github.com/biobuilds/chimerascan),
  [EricScript](https://sourceforge.net/projects/ericscript/files/), and
  [SoapFuse](https://sourceforge.net/p/soapfuse/wiki/Home/).

## FusionAnalyzer

Inputs:

- [input.csv](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/input.csv),
  here the files listed are the main output file from each caller.
- An example of the format of a generic fusion input:
  [generic_fusions.csv](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/generic_fusions.csv).

Outputs:

- The main output is scored/[final.candidates.csv](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/final.candidates.csv).

## FusionVisualizer

Inputs:

- [fv_input.csv](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/fv_input.csv),
  this file works for usage with/without --use-bp parameter.
  For usage without --use-bp only the first column is needed and the rest
  will be ignored if provided.

Outputs:

- Without --use-bp the output is the [FusionInspector](https://github.com/FusionInspector/FusionInspector).
- When --use-bp the output is a folder with two set of fusion references
  (virtual_ref1.fa and virtual_ref2.fa), annotations (annotation1.gtf,
  annotation1.gff and annotation2.gtf,annotation2.gff) and alignments
  (align1.sorted.bam and align2.sorted.bam). Set 1 has the genes involved
  in the fusions side by side; set 2 has the fusion recreated with the
  specified breakpoints. Check the
  [screenshot](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/fv_bp_igv_screenshot.pdf)
  to see the mappings to both virtual references in IGV.
  Example of [ouput folder](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/fungi_fv_SBF11_MAPK11_bps_50460534_50267949.tgz)
  for fusion SBF1--MAPK11 from our publication case study.

## FusionConsensus

Inputs:

- `--fusion_analyzer`: example: [final.candidates.csv](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/final.candidates.csv)
- `--input`: example: [input.csv](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/fcs_input.csv)
- `--fusion_list`: example: [input.csv](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/fcs_fusion_list.csv)


The outputs:

- [combined_fusions_report.csv](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/combined_fusions_report.csv): the combined results from --input and --fusion_analyzer input files.
- [bp_consensus_report.csv](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/bp_consensus_report.csv): the breakpoint consensus report.
