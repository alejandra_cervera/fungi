# Anduril instructions

For usage within [Anduril](https://anduril.org),
please follow Anduril's [download](https://anduril.org/site/download/)
instructions and consult the component [documentation](http://www.anduril.org/anduril/bundles/all/doc/)
for each component FusionCaller, FusionAnalyzer and FusionVisualizer.

Anduril has an install script for facilitating installation of required tools.
