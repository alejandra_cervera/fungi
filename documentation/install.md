# Installation

[TOC]


Instructions in this section are for FUNGI standalone version, for using
FUNGI within [Anduril](https://anduril.org) framework, see the
separate [installation/usage](anduril.md).

FUNGI installation has been tested on a Ubuntu 20.04 system.

## Download

```
git clone https://bitbucket.org/alejandra_cervera/fungi.git
```

- Add fungi/bin to your $PATH.
- Usage information and parameter description is displayed when running
  each module without parameters.


## Prepare config file

Create a template configuration file:

```
./fungi-prepare-config -c myConfig.txt
```

The command will create a config file that is needed for FUNGI to be
able to find the installation directories and databases used within FUNGI.


## Install dependencies

General dependencies:

- [STAR]("https://github.com/alexdobin/STAR") aligner
- R
- python

The minimum tools that need to be installed for each module:

- For `FusionCaller` at least one fusion calling method from: [STAR-Fusion]("https://github.com/STAR-Fusion/STAR-Fusion/wiki"), [FusionCatcher]("https://github.com/ndaniel/fusioncatcher"), [Arriba]("https://arriba.readthedocs.io/en/latest/"), [Chimerascan]("https://github.com/biobuilds/chimerascan"), [EricScript]("https://sourceforge.net/projects/ericscript/files/") or [SoapFuse]("href="https://sourceforge.net/p/soapfuse/wiki/Home/").
- For `FusionAnalyzer` you need [FusionCatcher]("https://github.com/ndaniel/fusioncatcher"), Pegasus and Oncofuse. Additionally you need to download Ensembl tables from our repository (instructions below).
- For `FusionVisualizer` you need [STAR-Fusion]("https://github.com/STAR-Fusion/STAR-Fusion/wiki").
- For `FusionConsensus` there is no additional dependencies.

Use the separate [instructions](dependencies.md) to install dependencies,
or follow the guides on the respective library pages.

Once you have installed the tools with their dependencies according to
each tool instructions and generated the required databases,
**add the install and db paths to the config file**.

A minimal very functional installation would include STAR, STAR-Fusion,
FusionCatcher, Pegasus and Oncofuse, and including Arriba would let you
use 3 state-of-the-art methods for fusion calling, do annotation and
scoring, visualization, and consensus breakpoint identification.

## Download resources

Run the download script to get all the necessary resources for running
and testing FUNGI.

```
cd resources
./download.sh
```

To ensure you have the latest versions, we recommend that you also
generate the required Ensembl tables: (Requires biomaRt package)

```
Rscript generate.R
```

The files updated with generate:
homolog_table.Rdata, GO.Rdata, geneid.Rdata


## Testcases

Test the scripts and tools configuration with [test cases](../tests/README.md).



