# Installation of dependency libraries

These commands were used to install dependencies. Tested in 2021/02.
This Document is based on the [Dockerfile](Dockerfile) in the repository.
Installation instructions for additional dependencies are included in [Dockerfile](Dockerfile) so do check it in case of trouble.
Here you can find instructions for the main tools.

[TOC]

## Arriba

```
wget https://github.com/suhrig/arriba/releases/download/v2.1.0/arriba_v2.1.0.tar.gz
tar -xzf arriba_v2.1.0.tar.gz
cd arriba_v2.1.0 && make
```

## SOAPFuse

```
wget 'https://sourceforge.net/projects/soapfuse/files/SOAPfuse_Package/SOAPfuse-v1.27.tar.gz'
tar xvzf SOAPfuse-v1.27.tar.gz
```

## FusionCatcher

### Using Github

```
git clone https://github.com/ndaniel/fusioncatcher
cd fusioncatcher/tools/
./install_tools.sh
cd ../data
./download-human-db.sh
```

### Alternative easier installation

```
sudo apt-get install wget gawk gcc g++ make cmake automake curl unzip \
     zip bzip2 tar gzip pigz parallel build-essential libncurses5-dev \
     libc6-dev zlib1g zlib1g-dev libtbb-dev libtbb2 python python-dev \
     python-numpy python-biopython python-xlrd python-openpyxl default-jdk
wget http://sf.net/projects/fusioncatcher/files/bootstrap.py -O bootstrap.py && \
  python bootstrap.py -t --download -y
```

## Chimerascan

```
git clone "https://github.com/genome/chimerascan-vrl.git" chimerascan-0.4.6
cd chimerascan-0.4.6
python setup.py build
python setup.py install --prefix $( pwd )       #needs root
```

### Download files

```
wget "http://hgdownload.cse.ucsc.edu/goldenPath/hg38/bigZips/hg38.fa.gz"
wget "http://hgdownload.cse.ucsc.edu/goldenpath/hg38/database/knownGene.txt.gz"
wget "http://hgdownload.cse.ucsc.edu/goldenpath/hg38/database/kgXref.txt.gz"
gunzip *.gz
```

### Create annotations

```
cut -f1-10 knownGene.txt > tmpKnownGene
cut -f5 kgXref.txt > tmpkgXref
paste tmpKnownGene tmpkgXref > UCSC.knownGene.txt
rm knownGene.txt tmpKnownGene kgXref.txt tmpkgXref
sed -i '1s/^/#name\tchrom\tstrand\ttxStart\ttxEnd\tcdsStart\tcdsEnd\texonCount\texonStarts\texonEnds\tgeneSymbol\n/' UCSC.knownGene.txt
```

### Build genome database
```
mkdir db
export PYTHONPATH=$( pwd )/lib/python2.7/site-packages:$PYTHONPATH
python chimerascan/chimerascan_index.py hg38.fa UCSC.knownGene.txt db
```

## STAR-Fusion

Here we include three possible ways of installing STAR-Fusion, but probably the most updated instructions are in STAR-Fusion's own documentation.

### The "Plug&Play method"
```
wget 'https://github.com/STAR-Fusion/STAR-Fusion/releases/download/STAR-Fusion-v1.9.0/STAR-Fusion.v1.9.0.FULL.tar.gz'
tar xvzf STAR-Fusion.v1.9.0.FULL.tar.gz
cd STAR-Fusion.v1.9.0
make

perl -MCPAN -e shell
   install DB_File
   install URI::Escape
   install Set::IntervalTree
   install Carp::Assert
   install JSON::XS
   install PerlIO::gzip
```

### Build from source

```
wget https://data.broadinstitute.org/Trinity/CTAT_RESOURCE_LIB/__genome_libs_StarFv1.9/GRCh38_gencode_v33_CTAT_lib_Apr062020.source.tar.gz
tar xvzf GRCh38_gencode_v33_CTAT_lib_Apr062020.source.tar.gz
cd GRCh38_gencode_v33_CTAT_lib_Apr062020.source
#from __build_ctat_genome_lib.sh copied the command below and ran it to build the index; i had to update trintiy and bowtie beforehand
perl  ../ctat-genome-lib-builder/prep_genome_lib.pl  --genome_fa GRCh38.primary_assembly.genome.fa --gtf gencode.v33.annotation.gtf --fusion_annot_lib CTAT_HumanFusionLib.dat.gz --annot_filter_rule AnnotFilterRule.pm --pfam_db current --dfam_db human --human_gencode_filter
```

### Alternative 

```
curl -jLf https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/lib/dfamscan.pl > /usr/local/bin/dfamscan.pl
chmod +x /usr/local/bin/dfamscan.pl 
git clone --recursive https://github.com/STAR-Fusion/STAR-Fusion.git
bash -c "cd STAR-Fusion && make clean && make"
```

## Oncofuse

```
curl -jLO https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/lib/oncofuse-1.1.1.zip 
unzip oncofuse-1.1.1.zip && rm oncofuse-1.1.1.zip
```

## Pegasus

```
# Pegasus requires old version of pandas (?)
curl -jL https://github.com/pandas-dev/pandas/releases/download/v0.24.2/pandas-0.24.2.tar.gz | tar xzv && \
    cd pandas-0.24.2 && \
    python2 setup.py install

git clone https://github.com/RabadanLab/Pegasus.git
cd Pegasus
wget https://bitbucket.org/alejandra_cervera/fungi/raw/master/scripts/format-Pegasus-gtf.pl
curl -L "ftp://ftp.ensembl.org/pub/release-84/gtf/homo_sapiens/Homo_sapiens.GRCh38.84.gtf.gz" | gunzip -c - > Homo_sapiens.GRCh38.84.gtf
curl -L "http://hgdownload.cse.ucsc.edu/goldenPath/hg38/bigZips/hg38.fa.gz" | gunzip -c - > hg38.fa
samtools faidx hg38.fa
cd learn
python train_model.py
cd ..
perl format-Pegasus-gtf.pl Homo_sapiens.GRCh38.84.gtf > annotations.gtf
rm Homo_sapiens.GRCh38.84.gtf
```
