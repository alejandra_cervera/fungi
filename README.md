#  FUsioN Genes Integration toolset (FUNGI)


FUNGI is a toolset wth 4 modules: FusionCaller, FusionAnalyzer, FusionVisualizer, and FusionConsensus

With FUNGI it is possible to:

- Call gene fusions from RNA-Seq with:
    - [STAR-Fusion](https://github.com/STAR-Fusion/STAR-Fusion/wiki)
    - [FusionCatcher](https://github.com/ndaniel/fusioncatcher)
    - [Arriba](https://arriba.readthedocs.io/en/latest/)
    - [ChimeraScan](https://github.com/biobuilds/chimerascan)
    - [EricScript](https://sourceforge.net/projects/ericscript/files/)
    - [SoapFuse](https://sourceforge.net/p/soapfuse/wiki/Home/)
- Integrate fusion calls from our supported methods or any fusions from other methods provided in a standard format.
- Filter, annotate and score your results to identify the most relevant fusions. Scoring is done with [Pegasus](https://github.com/RabadanLab/Pegasus) and [Oncofuse](https://github.com/mikessh/oncofuse).
- Recreate and visualize the fusions to facilitate validation and test for fusions in different sample sets.
- Find the consensus breakpoint from different tools on the same fusion genes (per sample).

Detailed description of FUNGI is included in [publication](https://academic.oup.com/bioinformatics/article/37/19/3353/6194566).


# Installation

Follow [Installation Documentation](documentation/install.md).

# Usage

See the [Usage Documentation](documentation/usage.md) for how to use, and examples of data files.

For using FUNGI with [Anduril](https://anduril.org),
follow [using with Anduril](documentation/anduril.md)

# How to add antigen annotation

We recommend to use [PVacFuse](https://pvactools.readthedocs.io/en/latest/pvacfuse.html) (part of pVacTools) for neoAntigen prediction for fusion genes. Check [AGFusion](https://pvactools.readthedocs.io/en/latest/pvacfuse/prerequisites.html#id1) for converting output fusions from FUNGI to a format compatible with pVacFuse.

# Publication results

FUNGI publication analyzes 107 samples from 36 patients of high-grade serous ovarian cancer.
Due to patient privacy concerns raw RNA-seq reads from the patients are not available, but all other inputs/outputs from the pipeline are included below.

The [pipeline](publication/paper_results_script.sh) includes all the analysis steps from the publication.

FusionCaller results from FUNGI publication case study:

- [STAR-Fusion calls](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/Publication/TarBalls/starfusion_calls.tgz)
- [SoapFuse calls](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/Publication/TarBalls/soapfuse_calls.tgz)
- [EricScript calls](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/Publication/TarBalls/ericscript_calls.tgz)
- [Chimerascan calls](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/Publication/TarBalls/chimerascan_calls.tgz)
- [FusionCatcher calls](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/Publication/TarBalls/fusioncatcher_calls.tgz)
- [Arriba calls](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/Publication/TarBalls/arriba_calls.tgz)

FusionAnalyzer results:

- [final.candidates.csv](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/Publication/final.candidates.csv)
- After filtering: [filtered_final_candidates.csv](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/Publication/filtered_final_candidates.csv) 

FusionVisualizer results:

- All samples combined & filtered: [fv_filtered.csv](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/Publication/fv_filtered.csv) 
- Example of visualization of one fusion with its exact breakpoint (--use-bp parameter) [screenshot](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/fv_bp_igv_screenshot.pdf) and [ouput folder](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/fungi_fv_SBF11_MAPK11_bps_50460534_50267949.tgz).


FusionConsensus results:

- FusionAnalyzer and FusionVisualizer combined results: [combined_fusion_report.csv](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/Publication/combined_fusions_report.csv)
- Majority breakpoint report: [bp_consensus_report.csv](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/Publication/bp_consensus_report.csv)
- Filtered combined report: [final_filtered_fusion_report.csv](https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/Publication/final_filtered_fusion_report.csv)

