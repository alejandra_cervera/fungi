#!/bin/bash
set -e

printf "\033[33;1m%s\033[0m\n" "Checking for downloadable assets 💾"

function curlget() {
    printf "."
    test -s "$1" || {
        printf "\033[33m%s\033[0m\n" "Downloading $1"
        curl -fjL -o "$1" "$2" || rm "$1"
    }
}

BB=https://csbi.ltdk.helsinki.fi/pub/projects/FUNGI/Files/resources/
for file in GO.Rdata geneid.Rdata homolog_table.Rdata sessionInfo_ensembl_version.txt \
            reads.fq.gz mates.fq.gz \
            fa_input.csv fv_input.csv generic_fusions.csv \
            fcs_input.csv fcs_fusion_analyzer.csv fcs_fusion_list.csv; do
    if [[ "$1" = clean ]]; then
        rm -f -v "$file"
        continue
    fi

    curlget "$file" "$BB"/"$file"
done
echo
